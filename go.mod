module gitlab.com/passelecasque/obstruction

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/anacrolix/missinggo v1.2.1
	github.com/anacrolix/torrent v1.9.0
	github.com/mmcdole/gofeed v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/catastrophic/assistance v0.29.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
)

go 1.13
