# obstruction

[![Documentation](https://godoc.org/gitlab.com/passelecasque/obstruction?status.svg)](https://godoc.org/gitlab.com/passelecasque/obstruction)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/passelecasque/obstruction)](https://goreportcard.com/report/gitlab.com/passelecasque/obstruction) [![codecov](https://codecov.io/gl/passelecasque/obstruction/branch/master/graph/badge.svg)](https://codecov.io/gl/passelecasque/obstruction) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/passelecasque/obstruction/badges/master/build.svg)](https://gitlab.com/passelecasque/obstruction/pipelines)

A number of generally helpful Go functions and packages.
