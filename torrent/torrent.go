package torrent

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/anacrolix/missinggo/slices"
	"github.com/anacrolix/torrent/bencode"
	"github.com/anacrolix/torrent/metainfo"
	"gitlab.com/catastrophic/assistance/fs"
)

// newTrue allows setting the value of a *bool in a struct.
// it is arguably awful.
func newTrue() *bool {
	b := true
	return &b
}

type Torrent struct {
	Target   string
	Filename string
}

func New(target string) *Torrent {
	return &Torrent{Target: target}
}

// This is a helper that sets Files and Pieces from a root path and its children.
// forked from metainfo.BuildFromFilePath to allow excluding files and folders
func (t *Torrent) buildFromFilePath(info *metainfo.Info, root string, excludedSubdirs []string) (err error) {
	info.Name = filepath.Base(root)
	info.Files = nil
	err = filepath.Walk(root, func(path string, fi os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if fi.IsDir() {
			// Directories are implicit in torrent files.
			return nil
		} else if path == root {
			// The root is a file.
			info.Length = fi.Size()
			return nil
		}
		relPath, err := filepath.Rel(root, path)
		if err != nil {
			return fmt.Errorf("error getting relative path: %s", err)
		}
		// excluding files from forbidden subdirs
		for _, subd := range excludedSubdirs {
			if strings.HasPrefix(relPath, subd) {
				return nil
			}
		}
		info.Files = append(info.Files, metainfo.FileInfo{
			Path:   strings.Split(relPath, string(filepath.Separator)),
			Length: fi.Size(),
		})
		return nil
	})
	if err != nil {
		return
	}
	slices.Sort(info.Files, func(l, r metainfo.FileInfo) bool {
		return strings.Join(l.Path, "/") < strings.Join(r.Path, "/")
	})
	err = info.GeneratePieces(func(fi metainfo.FileInfo) (io.ReadCloser, error) {
		return os.Open(filepath.Join(root, strings.Join(fi.Path, string(filepath.Separator))))
	})
	if err != nil {
		err = fmt.Errorf("error generating pieces: %s", err)
	}
	return err
}

func (t *Torrent) Generate(torrentPath, comment, source string, announceURLs []string, origin string, excludedSubdirs []string) error {
	if !fs.FileExists(t.Target) && !fs.DirExists(t.Target) {
		// TODO check dir not empty fs.DirIsEmpty, if relevant
		return errors.New("source file(s) cannot be found")
	}
	t.Filename = torrentPath
	if fs.FileExists(t.Filename) {
		return errors.New("torrent " + t.Filename + " already exists")
	}

	al := metainfo.AnnounceList{}
	for _, url := range announceURLs {
		al = append(al, []string{url})
	}
	mi := metainfo.MetaInfo{AnnounceList: al}
	mi.Comment = comment
	mi.CreatedBy = origin
	mi.CreationDate = time.Now().Unix()
	i := metainfo.Info{PieceLength: 256 * 1024, Private: newTrue(), Source: source}
	if err := t.buildFromFilePath(&i, t.Target, excludedSubdirs); err != nil {
		return err
	}
	bytes, err := bencode.Marshal(i)
	if err != nil {
		return err
	}
	mi.InfoBytes = bytes
	f, err := os.Create(torrentPath)
	if err != nil {
		return err
	}
	defer f.Close()
	return mi.Write(f)
}

func (t *Torrent) Seed(watchDir, seedDir string) error {
	// copy files to seed dir
	seedPath := filepath.Join(seedDir, filepath.Base(t.Target))
	if fs.DirExists(t.Target) {
		if fs.DirExists(seedPath) {
			return errors.New("folder to seed is already in the relevant directory")
		}
		if err := fs.CopyDir(t.Target, seedPath, false); err != nil {
			return err
		}
	}
	if fs.FileExists(t.Target) {
		if fs.FileExists(seedPath) {
			return errors.New("file to seed is already in the relevant directory")
		}
		if err := fs.CopyFile(t.Target, seedPath, false); err != nil {
			return err
		}
	}
	// copy torrent to watch dir
	if err := fs.CopyFile(t.Filename, filepath.Join(watchDir, filepath.Base(t.Filename)), false); err != nil {
		return err
	}
	return nil
}

func (t *Torrent) Archive(archiveDir string) error {
	// copy files to seed dir
	archivePath := filepath.Join(archiveDir, filepath.Base(t.Target))
	if fs.DirExists(t.Target) {
		if fs.DirExists(archivePath) {
			return errors.New("folder to seed is already in the relevant directory")
		}
		if err := fs.CopyDir(t.Target, archivePath, false); err != nil {
			return err
		}
	}
	if fs.FileExists(t.Target) {
		if fs.FileExists(archivePath) {
			return errors.New("file to seed is already in the relevant directory")
		}
		if err := fs.CopyFile(t.Target, archivePath, false); err != nil {
			return err
		}
	}
	if err := os.RemoveAll(t.Target); err != nil {
		return err
	}
	// move torrent to watch dir
	if err := fs.CopyFile(t.Filename, filepath.Join(archiveDir, filepath.Base(t.Filename)), false); err != nil {
		return err
	}
	return os.RemoveAll(t.Filename)
}

func (t *Torrent) DeleteFiles() error {
	if err := os.RemoveAll(t.Target); err != nil {
		return err
	}
	return os.RemoveAll(t.Filename)
}
