package tracker

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

type LogChecker struct {
	Issues []string `json:"issues"`
	Score  int      `json:"score"`
}

func (lc *LogChecker) String() string {
	pattern := "Score: %d"
	if len(lc.Issues) != 0 {
		pattern += " | Comments: %s"
		return fmt.Sprintf(pattern, lc.Score, strings.Join(lc.Issues, ", "))
	}
	return fmt.Sprintf(pattern, lc.Score)
}

type LogCheckerResponse struct {
	Status   string     `json:"status"`
	Error    string     `json:"error"`
	Response LogChecker `json:"response"`
}

const (
	UserTorrentsSnatched = "snatched"
	UserTorrentsLeeching = "leeching"
	UserTorrentsUploaded = "uploaded"
	UserTorrentsSeeding  = "seeding"
)

func IsValidTorrentType(tt string) bool {
	return strslice.Contains([]string{UserTorrentsSnatched, UserTorrentsLeeching, UserTorrentsUploaded, UserTorrentsSeeding}, tt)
}

type UserTorrent struct {
	ArtistID   int    `json:"artistId"`
	ArtistName string `json:"artistName"`
	GroupID    int    `json:"groupId"`
	Name       string `json:"name"`
	TorrentID  int    `json:"torrentId"`
}

type UserTorrents struct {
	Seeding  []UserTorrent `json:"seeding"`
	Leeching []UserTorrent `json:"leeching"`
	Uploaded []UserTorrent `json:"uploads"`
	Snatched []UserTorrent `json:"snatched"`
}

type UserTorrentsResponse struct {
	Status   string       `json:"status"`
	Error    string       `json:"error"`
	Response UserTorrents `json:"response"`
}

type FillRequest struct {
	Bounty      int    `json:"bounty"`
	Fillerid    int    `json:"fillerid"`
	Fillername  string `json:"fillername"`
	Requestid   int    `json:"requestid"`
	Requestname string `json:"requestname"`
	Torrentid   int    `json:"torrentid"`
}

type FillRequestResponse struct {
	Status   string      `json:"status"`
	Error    string      `json:"error"`
	Response FillRequest `json:"response"`
}

// IntOrString is defined because the response should be an int, but is sometimes a string.
type IntOrString int

// UnmarshalJSON allows to unmarshall JSON with an int field sometimes returned as string.
func (w *IntOrString) UnmarshalJSON(data []byte) error {
	// !! not using IntOrString directly, otherwise it loops
	var v int
	// trying to unmarshall normally
	if err := json.Unmarshal(data, &v); err != nil {
		// not an int, now trying to read as a string
		var t string
		// if it's not a string either, really return an error
		if err := json.Unmarshal(data, &t); err != nil {
			return err
		}
		// if it's a string, trying to get the int conversion
		v, err = strconv.Atoi(t)
		// if not an int, really return an error
		if err != nil {
			return err
		}
		// if it's an int, converting
	}
	*w = IntOrString(v)
	return nil
}

type UploadResult struct {
	Private   bool `json:"private"`
	Source    bool `json:"source"`
	RequestID int  `json:"requestid"`
	TorrentID int  `json:"torrentid"`
	GroupID   int  `json:"groupid"`
}

type UploadResultResponse struct {
	Status   string       `json:"status"`
	Error    string       `json:"error"`
	Response UploadResult `json:"response"`
}

// TagOrSlice is defined because the response should be a []string or a struct
type TagOrSlice []string

// UnmarshalJSON allows to unmarshall JSON for tags
func (w *TagOrSlice) UnmarshalJSON(data []byte) error {
	var v []string
	// trying to unmarshall normally
	if err := json.Unmarshal(data, &v); err != nil {
		var t []struct {
			Tag string `json:"tagname"`
		}
		// if it's not a bool either, really return an error
		if err := json.Unmarshal(data, &t); err != nil {
			return err
		}
		// converting into a []string
		for _, el := range t {
			v = append(v, el.Tag)
		}
	}
	*w = TagOrSlice(v)
	return nil
}

type AddTag struct {
	Added    TagOrSlice `json:"added"`
	Rejected TagOrSlice `json:"rejected"`
	Voted    TagOrSlice `json:"voted"`
}

type AddTagResponse struct {
	Status   string `json:"status"`
	Error    string `json:"error"`
	Response AddTag `json:"response"`
}
