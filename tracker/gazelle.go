package tracker

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	statusSuccess = "success"

	deletedFromTrackerPattern = `<span class="log_deleted"> Torrent <a href="torrents\.php\?torrentid=\d+">(\d+)</a>(.*)</span>`

	errorGazelleRateLimitExceeded = "rate limit exceeded"
	errorJSONAPI                  = "Error calling JSON API"
	errorCouldNotMoveTorrent      = "Error moving torrent to destination folder"
	errorRemovingTempFile         = "Error removing temporary file %s"
	errorUnmarshallingJSON        = "could not parse JSON response"
	errorInvalidResponse          = "could not parse response"
	errorAPIResponseStatus        = "unsuccessful API call: "
	errorUnknownTorrentURL        = "missing torrent url"
	errorCouldNotCreateForm       = "Could not create form for log"
	errorCouldNotReadLog          = "Could not read log"
)

// Gazelle allows querying the Gazelle JSON API.
type Gazelle struct {
	Tracker
	apiURL      string
	UserID      int
	improvedAPI bool
}

func NewGazelle(name, domain, user, password, cookieID, cookieValue, apiKey, userAgent string) (*Gazelle, error) {
	t := &Gazelle{}
	if err := t.Init(name, domain, user, password, cookieID, cookieValue, strings.Replace(domain, "https://", "", -1), apiKey, userAgent, domain+"/login.php", true); err != nil {
		return nil, err
	}
	t.apiURL = t.DomainURL + "/ajax.php?"

	return t, nil
}

func (t *Gazelle) Login() error {
	data, err := t.login()
	if err != nil {
		logthis.Info("Returned when attempting to log in: "+data, logthis.VERBOSESTEST)
		return err
	}
	// getting user-specific information
	idx, err := t.GetIndex()
	if err != nil {
		return err
	}
	t.AuthKey = idx.Authkey
	t.UserID = idx.ID
	t.Passkey = idx.Passkey
	t.User = idx.Username
	// unlocks new RED-specific API features
	t.improvedAPI = idx.APIVersion != ""
	return nil
}

func (t *Gazelle) checkResponseStatus(data []byte) error {
	// check success
	var r GazelleGenericResponse
	if err := json.Unmarshal(data, &r); err != nil {
		// logthis.Info("BAD JSON, Received: \n"+string(data), logthis.VERBOSEST)
		return errors.New(errorUnmarshallingJSON)
	}
	if r.Status != statusSuccess {
		if r.Status == "" {
			return errors.New(errorInvalidResponse)
		}
		if r.Error == errorGazelleRateLimitExceeded {
			return errors.New(errorGazelleRateLimitExceeded)
		}
		return errors.New(errorAPIResponseStatus + r.Status + " : " + r.Error)
	}
	return nil
}

// rateLimitedAPICall make a rate-limited GET or POST API call, checking the status and error in the JSON, automatically retrying if over the rate limit.
func (t *Gazelle) rateLimitedAPICall(url, method string, b io.Reader, contentType string, expectingJSON, apiKeySupported bool) ([]byte, error) {
	var data []byte
	var err error
	var buf bytes.Buffer
	tee := io.TeeReader(b, &buf)
	switch method {
	case http.MethodGet:
		data, _, err = t.getRequestRateLimited(url)
	case http.MethodPost:
		data, _, err = t.postRequestRateLimited(url, tee, contentType, apiKeySupported)
	default:
		return []byte{}, errors.New("unknown request method: " + method)
	}

	// checking if error besides rate limit being hit
	if err != nil && err.Error() != fmt.Sprintf("%d %s", http.StatusTooManyRequests, http.StatusText(http.StatusTooManyRequests)) {
		return data, err
	}

	if expectingJSON {
		// checking success
		if err := t.checkResponseStatus(data); err != nil {
			if err.Error() == errorGazelleRateLimitExceeded {
				logthis.Info(errorJSONAPI+": "+errorGazelleRateLimitExceeded+", retrying.", logthis.NORMAL)
				// calling again, waiting for the rate limiter again should do the trick.
				// that way 2 limiter slots will have passed before the next call is made,
				// the server should allow it.
				if err := t.waitForLimiter(); err != nil {
					return []byte{}, err
				}
				return t.rateLimitedAPICall(url, method, &buf, contentType, expectingJSON, apiKeySupported)
			}
			return data, err
		}
		return data, nil
	}
	return data, nil
}

// unmarshalAPICall sends a HTTP GET request to the API and decodes the JSON response into responseObj.
func (t *Gazelle) unmarshalAPICall(endpoint string, responseObj interface{}) error {
	data, err := t.rateLimitedAPICall(t.apiURL+endpoint, http.MethodGet, nil, "", true, true)
	if err != nil {
		if len(data) != 0 {
			if responseErr := t.checkResponseStatus(data); responseErr != nil {
				return errors.New(responseErr.Error() + " (" + err.Error() + ")")
			}
		}
		return errors.Wrap(err, errorJSONAPI)
	}
	return json.Unmarshal(data, responseObj)
}

// unmarshalAPICall sends a HTTP POST request to the API and decodes the JSON response into responseObj.
func (t *Gazelle) unmarshalPOSTAPICall(endpoint string, b io.Reader, contentType string, apiKeySupported bool, responseObj interface{}) error {
	data, err := t.rateLimitedAPICall(t.apiURL+endpoint, http.MethodPost, b, contentType, true, apiKeySupported)
	if err != nil {
		if len(data) != 0 {
			if responseErr := t.checkResponseStatus(data); responseErr != nil {
				return errors.New(responseErr.Error() + " (" + err.Error() + ")")
			}
		}
		return errors.Wrap(err, errorJSONAPI)
	}
	return json.Unmarshal(data, responseObj)
}
