package tracker

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type HTTPBinResponse struct {
	Args    struct{} `json:"args"`
	Headers struct {
		AcceptEncoding string `json:"Accept-Encoding"`
		Connection     string `json:"Connection"`
		Host           string `json:"Host"`
		UserAgent      string `json:"User-Agent"`
	} `json:"headers"`
	Origin string `json:"origin"`
	URL    string `json:"url"`
}

func TestTrackerAPI(t *testing.T) {
	fmt.Println("+ Testing Tracker...")

	// setting up
	check := assert.New(t)
	//trckr := &Tracker{Name: "test TRKR", DomainURL: "http://httpbin.org", User: "user", Password: "password", limiter: make(chan bool, defaultAllowedAPICallsByPeriod), limiterAvailable: make(chan bool, 1), UserAgent: "useragent/120", rateLimitCallByPeriod: defaultAllowedAPICallsByPeriod,
	trckr := &Tracker{}
	check.Nil(trckr.Init("test TRKR", "http://httpbin.org", "user", "password", "", "", "", "", "useragent/120", "", true))
	trckr.Client = &http.Client{}
	trckr.StartRateLimiter()

	fmt.Println("Creating fake async by waiting for 5s: 5 reqs will be sent, and we'll need to wait for another 5s in the next 10s slot before sending the next reqs!")
	//time.Sleep(5000 * time.Millisecond)
	time.Sleep(50 * time.Millisecond)

	// get time
	start := time.Now()

	for i := 0; i < 11; i++ {
		//fmt.Printf("Waiting to send GET #%d\n", i+1)
		data, respURL, err := trckr.getRequestRateLimited(trckr.DomainURL + "/get")
		check.Equal(trckr.DomainURL+"/get", respURL)
		fmt.Printf("%s: Sent GET #%d\n", time.Since(start).String(), i+1)
		check.Nil(err)
		var r HTTPBinResponse
		check.Nil(json.Unmarshal(data, &r))
		check.Equal("useragent/120", r.Headers.UserAgent)
	}

	elapsed := time.Since(start)
	fmt.Printf("11 GET calls in %s\n", elapsed.String())
	check.True(elapsed.Seconds() > 20)
	fmt.Println("If the 11th call is sent after 20s, it means 10 calls were sent in 20s, i.e. we have successfully rate-limited to 5reqs/10s")
}
