package tracker

import (
	"encoding/json"
	"strings"

	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	FormatFLAC = "FLAC"
	FormatMP3  = "MP3"
	FormatAAC  = "AAC"
	FormatAC3  = "AC3"
	FormatDTS  = "DTS"
	FormatDSD  = "DSD"

	SourceCD         = "CD"
	SourceWEB        = "WEB"
	SourceDVD        = "DVD"
	SourceVinyl      = "Vinyl"
	SourceSoundboard = "Soundboard"
	SourceDAT        = "DAT"
	SourceCassette   = "Cassette"
	SourceBluRay     = "Blu-Ray"
	SourceSACD       = "SACD"

	Quality192           = "192"
	Quality256           = "256"
	Quality320           = "320"
	QualityAPS           = "APS (VBR)"
	QualityAPX           = "APX (VBR)"
	QualityV2            = "V2 (VBR)"
	QualityV1            = "V1 (VBR)"
	QualityV0            = "V0 (VBR)"
	QualityLossless      = "Lossless"
	Quality24bitLossless = "24bit Lossless"
	QualityDSD64         = "DSD64"
	QualityDSD128        = "DSD128"
	QualityDSD256        = "DSD256"
	QualityDSD512        = "DSD512"

	ReleaseAlbum       = "Album"
	ReleaseSoundtrack  = "Soundtrack"
	ReleaseEP          = "EP"
	ReleaseAnthology   = "Anthology"
	ReleaseCompilation = "Compilation"
	ReleaseSingle      = "Single"
	ReleaseLive        = "Live album"
	ReleaseRemix       = "Remix"
	ReleaseBootleg     = "Bootleg"
	ReleaseInterview   = "Interview"
	ReleaseMixtape     = "Mixtape"
	ReleaseDemo        = "Demo"
	ReleaseConcert     = "Concert Recording"
	ReleaseDJMix       = "DJ Mix"
	ReleaseUnknown     = "Unknown"

	RoleMain      = "Main"
	RoleGuest     = "Guest"
	RoleComposer  = "Composer"
	RoleConductor = "Conductor"
	RoleDJ        = "DJ / Compiler"
	RoleRemixer   = "Remixer"
	RoleProducer  = "Producer"

	TrackPattern   = `(.*[.flac|.FLAC|.mp3|.MP3]){{{(\d*)}}}`
	VariousArtists = "Various Artists"

	ClassUser            = "User"
	ClassMember          = "Member"
	ClassPowerUser       = "Power User"
	ClassElite           = "Elite"
	ClassTorrentMaster   = "Torrent Master"
	ClassPowerTM         = "Power TM"
	ClassEliteTM         = "Elite TM"
	ClassVIP             = "VIP"
	ClassModerator       = "Moderator"
	ClassSeniorModerator = "Senior Moderator"
	ClassAdministrator   = "Administrator"
	ClassDeveloper       = "Developer"
	ClassLegend          = "Legend"
	ClassSysOp           = "Sysop"
)

var (
	releaseTypes = map[int]string{
		1:  ReleaseAlbum,
		3:  ReleaseSoundtrack,
		5:  ReleaseEP,
		6:  ReleaseAnthology,
		7:  ReleaseCompilation,
		9:  ReleaseSingle,
		11: ReleaseLive,
		13: ReleaseRemix,
		14: ReleaseBootleg,
		15: ReleaseInterview,
		16: ReleaseMixtape,
		17: ReleaseDemo,
		18: ReleaseConcert,
		19: ReleaseDJMix,
		21: ReleaseUnknown,
	}
	artistRoles = map[int]string{
		1: RoleMain,
		2: RoleGuest,
		3: RoleRemixer,
		4: RoleComposer,
		5: RoleConductor,
		6: RoleDJ,
		7: RoleProducer,
	}
	KnownReleaseTypes = []string{ReleaseAlbum, ReleaseSoundtrack, ReleaseEP, ReleaseAnthology, ReleaseCompilation, ReleaseSingle, ReleaseLive, ReleaseRemix, ReleaseBootleg, ReleaseInterview, ReleaseMixtape, ReleaseDemo, ReleaseConcert, ReleaseDJMix, ReleaseUnknown}
	KnownFormats      = []string{FormatFLAC, FormatMP3, FormatAAC, FormatAC3, FormatDTS, FormatDSD}
	KnownSources      = []string{SourceCD, SourceWEB, SourceVinyl, SourceBluRay, SourceCassette, SourceDVD, SourceDAT, SourceSoundboard, SourceSACD}
	KnownQualities    = []string{Quality192, Quality256, Quality320, QualityAPS, QualityAPX, QualityV2, QualityV1, QualityV0, QualityLossless, Quality24bitLossless, QualityDSD64, QualityDSD128, QualityDSD256, QualityDSD512}
)

func GazelleReleaseType(value int) string {
	label, ok := releaseTypes[value]
	if !ok {
		return "Unknown value"
	}
	return label
}

func GazelleReleaseTypeByLabel(value string) int {
	for i, v := range releaseTypes {
		// case insensitive
		if strings.EqualFold(v, value) {
			return i
		}
	}
	return 0
}

func GazelleArtistRole(value int) string {
	label, ok := artistRoles[value]
	if !ok {
		return "Unknown value"
	}
	return label
}

func GazelleArtistRoleByLabel(value string) int {
	for i, v := range artistRoles {
		// case insensitive
		if strings.EqualFold(v, value) {
			return i
		}
	}
	return 0
}

func ShortEncoding(encoding string) string {
	var format string
	switch encoding {
	case QualityLossless:
		format = "FLAC"
	case Quality24bitLossless:
		format = "FLAC24"
	case QualityV0:
		format = "V0"
	case QualityV2:
		format = "V2"
	case Quality320:
		format = "320"
	default:
		format = "UnF"
	}
	return format
}

func ShortEdition(edition string) string {
	editionReplacer := strings.NewReplacer(
		"Reissue", "RE",
		"Repress", "RP",
		"Remaster", "RM",
		"Remastered", "RM",
		"Limited Edition", "LTD",
		"Deluxe Edition", "DLX",
		"Deluxe", "DLX",
		"Special Edition", "SE",
		"Bonus Tracks", "Bonus",
		"Bonus Tracks Edition", "Bonus",
		"Promo", "PR",
		"Test Pressing", "TP",
		"Self Released", "SR",
		"Box Set", "Box",
		"Compact Disc Recordable", "CDr",
		"Japan Edition", "Japan",
		"Japan Release", "Japan",
	)
	return editionReplacer.Replace(edition)
}

type GazelleGenericResponse struct {
	Response interface{} `json:"response"`
	Status   string      `json:"status"`
	Error    string      `json:"error"`
}

type GazelleIndex struct {
	APIVersion    string `json:"api_version"`
	Authkey       string `json:"authkey"`
	ID            int    `json:"id"`
	Notifications struct {
		Messages         int  `json:"messages"`
		NewAnnouncement  bool `json:"newAnnouncement"`
		NewBlog          bool `json:"newBlog"`
		NewSubscriptions bool `json:"newSubscriptions"`
		Notifications    int  `json:"notifications"`
	} `json:"notifications"`
	Passkey   string `json:"passkey"`
	Username  string `json:"username"`
	UserStats struct {
		Class         string  `json:"class"`
		Downloaded    int     `json:"downloaded"`
		Ratio         float64 `json:"ratio"`
		RequiredRatio float64 `json:"requiredratio"`
		Uploaded      int     `json:"uploaded"`
	} `json:"userstats"`
}

type GazelleIndexResponse struct {
	Response GazelleIndex `json:"response"`
	Status   string       `json:"status"`
	Error    string       `json:"error"`
}

// ProfileAlbum is defined because the response is either this struct, or an empty slice.
type ProfileAlbum struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Review string `json:"review"`
}

// UnmarshalJSON allows to unmarshall JSON with an empty slice as profile album, without throwing errors.
// Instead, the prfile album return an empty map.
func (w *ProfileAlbum) UnmarshalJSON(data []byte) error {
	// !! not using ProfileAlbum directly, otherwise it loops
	var v struct {
		ID     string `json:"id"`
		Name   string `json:"name"`
		Review string `json:"review"`
	}
	// trying to unmarshall normally
	if err := json.Unmarshal(data, &v); err != nil {
		// not a map, now trying to read as a slice
		var t []string
		// if it's not a slice either, really return an error
		if err := json.Unmarshal(data, &t); err != nil {
			return err
		}
		// if it's a slice, it's an empty slice. returning the empty struct.
	}
	*w = ProfileAlbum(v)
	return nil
}

type GazelleUserStats struct {
	Avatar    string `json:"avatar"`
	Community struct {
		ArtistsAdded    int   `json:"artistsAdded"`
		ArtistComments  int   `json:"artistComments"`
		BountyEarned    int   `json:"bountyEarned"`
		BountySpent     int64 `json:"bountySpent"`
		CollageComments int   `json:"collageComments"`
		CollagesContrib int   `json:"collagesContrib"`
		CollagesStarted int   `json:"collagesStarted"`
		Groups          int   `json:"groups"`
		GroupVotes      int   `json:"groupVotes"`
		Invited         int   `json:"invited"`
		Leeching        int   `json:"leeching"`
		PerfectFlacs    int   `json:"perfectFlacs"`
		Posts           int   `json:"posts"`
		RequestComments int   `json:"requestComments"`
		RequestsFilled  int   `json:"requestsFilled"`
		RequestsVoted   int   `json:"requestsVoted"`
		Seeding         int   `json:"seeding"`
		Snatched        int   `json:"snatched"`
		TorrentComments int   `json:"torrentComments"`
		Uploaded        int   `json:"uploaded"`
	} `json:"community"`
	IsFriend bool `json:"isFriend"`
	Personal struct {
		Class        string `json:"class"`
		Donor        bool   `json:"donor"`
		Enabled      bool   `json:"enabled"`
		Paranoia     int    `json:"paranoia"`
		ParanoiaText string `json:"paranoiaText"`
		Passkey      string `json:"passkey"`
		Warned       bool   `json:"warned"`
	} `json:"personal"`
	ProfileAlbum ProfileAlbum `json:"profileAlbum,omitempty"`
	ProfileText  string       `json:"profileText"`
	Ranks        struct {
		Artists    int         `json:"artists"`
		Bounty     int         `json:"bounty"`
		Downloaded int         `json:"downloaded"`
		Overall    interface{} `json:"overall"`
		Posts      int         `json:"posts"`
		Requests   int         `json:"requests"`
		Uploaded   int         `json:"uploaded"`
		Uploads    int         `json:"uploads"`
	} `json:"ranks"`
	Stats struct {
		Buffer        int64   `json:"buffer"`
		Downloaded    uint64  `json:"downloaded"`
		JoinedDate    string  `json:"joinedDate"`
		LastAccess    string  `json:"lastAccess"`
		Ratio         float64 `json:"ratio"`
		RequiredRatio float64 `json:"requiredRatio"`
		Uploaded      uint64  `json:"uploaded"`
	} `json:"stats"`
	Username string `json:"username"`
}

type GazelleUserStatsResponse struct {
	Response GazelleUserStats `json:"response"`
	Status   string           `json:"status"`
	Error    string           `json:"error"`
}

type Artist struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type CollageInfo struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	NumTorrents int    `json:"numTorrents"`
}

type GazelleTorrentGroup struct {
	Group struct {
		BbBody          string        `json:"bbBody"`
		CatalogueNumber string        `json:"catalogueNumber"`
		CategoryID      int           `json:"categoryId"`
		CategoryName    string        `json:"categoryName"`
		Collages        []CollageInfo `json:"collages"`
		ID              int           `json:"id"`
		IsBookmarked    bool          `json:"isBookmarked"`
		MusicInfo       struct {
			Artists   []Artist `json:"artists"`
			Composers []Artist `json:"composers"`
			Conductor []Artist `json:"conductor"`
			Dj        []Artist `json:"dj"`
			Producer  []Artist `json:"producer"`
			RemixedBy []Artist `json:"remixedBy"`
			With      []Artist `json:"with"`
		} `json:"musicInfo"`
		Name             string        `json:"name"`
		PersonalCollages []CollageInfo `json:"personalCollages"`
		RecordLabel      string        `json:"recordLabel"`
		ReleaseType      int           `json:"releaseType"`
		Tags             []string      `json:"tags"`
		Time             string        `json:"time"`
		VanityHouse      bool          `json:"vanityHouse"`
		WikiBody         string        `json:"wikiBody"`
		WikiImage        string        `json:"wikiImage"`
		Year             int           `json:"year"`
	} `json:"group"`
	Torrents []struct {
		Description             string `json:"description"`
		Encoding                string `json:"encoding"`
		FileCount               int    `json:"fileCount"`
		FileList                string `json:"fileList"`
		FilePath                string `json:"filePath"`
		Format                  string `json:"format"`
		FreeTorrent             bool   `json:"freeTorrent"`
		HasCue                  bool   `json:"hasCue"`
		HasLog                  bool   `json:"hasLog"`
		HasSnatched             bool   `json:"has_snatched"`
		ID                      int    `json:"id"`
		Leechers                int    `json:"leechers"`
		LogScore                int    `json:"logScore"`
		LossyMasterApproved     bool   `json:"lossyMasterApproved"`
		LossyWebApproved        bool   `json:"lossyWebApproved"`
		Media                   string `json:"media"`
		RemasterCatalogueNumber string `json:"remasterCatalogueNumber"`
		RemasterRecordLabel     string `json:"remasterRecordLabel"`
		RemasterTitle           string `json:"remasterTitle"`
		RemasterYear            int    `json:"remasterYear"`
		Remastered              bool   `json:"remastered"`
		Reported                bool   `json:"reported"`
		Scene                   bool   `json:"scene"`
		Seeders                 int    `json:"seeders"`
		Size                    int    `json:"size"`
		Snatched                int    `json:"snatched"`
		Time                    string `json:"time"`
		Trumpable               bool   `json:"trumpable"`
		UserID                  int64  `json:"userId"`
		Username                string `json:"username"`
	} `json:"torrents"`
}

func (gtg *GazelleTorrentGroup) Anonymize() {
	for i := range gtg.Torrents {
		gtg.Torrents[i].UserID = 0
		gtg.Torrents[i].Username = ""
	}
}

func (gtg *GazelleTorrentGroup) AlreadySnatched() bool {
	for _, t := range gtg.Torrents {
		if t.HasSnatched {
			return true
		}
	}
	return false
}

type GazelleTorrentGroupResponse struct {
	Response GazelleTorrentGroup `json:"response"`
	Status   string              `json:"status"`
	Error    string              `json:"error"`
}

type ExtendedArtists struct {
	One []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"1"`
	Two []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"2"`
	Three []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"3"`
	Four []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"4"`
	Five []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"5"`
	Six []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"6"`
	Seven []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"7"`
}

// UnmarshalJSON allows to unmarshall JSON with an empty slice as profile album, without throwing errors.
// Instead, the prfile album return an empty map.
func (w *ExtendedArtists) UnmarshalJSON(data []byte) error {
	// !! not using ProfileAlbum directly, otherwise it loops
	var v struct {
		One []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"1"`
		Two []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"2"`
		Three []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"3"`
		Four []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"4"`
		Five []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"5"`
		Six []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"6"`
		Seven []struct {
			Aliasid int    `json:"aliasid"`
			ID      int    `json:"id"`
			Name    string `json:"name"`
		} `json:"7"`
	}
	// trying to unmarshall normally
	if err := json.Unmarshal(data, &v); err != nil {
		// not a map, now trying to read as a bool
		var t bool
		// if it's not a slice either, really return an error
		if err := json.Unmarshal(data, &t); err != nil {
			return err
		}
		// if it's a slice, it's an empty slice. returning the empty struct.
	}
	*w = ExtendedArtists(v)
	return nil
}

type GazelleArtist struct {
	Body                 string `json:"body"`
	HasBookmarked        bool   `json:"hasBookmarked"`
	ID                   int    `json:"id"`
	Image                string `json:"image"`
	Name                 string `json:"name"`
	NotificationsEnabled bool   `json:"notificationsEnabled"`
	Requests             []struct {
		Bounty     int    `json:"bounty"`
		CategoryID int    `json:"categoryId"`
		RequestID  int    `json:"requestId"`
		TimeAdded  string `json:"timeAdded"`
		Title      string `json:"title"`
		Votes      int    `json:"votes"`
		Year       int    `json:"year"`
	} `json:"requests"`
	SimilarArtists []struct {
		ArtistID  int    `json:"artistId"`
		Name      string `json:"name"`
		Score     int    `json:"score"`
		SimilarID int    `json:"similarId"`
	} `json:"similarArtists"`
	Statistics struct {
		NumGroups   int `json:"numGroups"`
		NumLeechers int `json:"numLeechers"`
		NumSeeders  int `json:"numSeeders"`
		NumSnatches int `json:"numSnatches"`
		NumTorrents int `json:"numTorrents"`
	} `json:"statistics"`
	Tags []struct {
		Count int    `json:"count"`
		Name  string `json:"name"`
	} `json:"tags"`
	Torrentgroup []GazelleArtistTorrentGroup `json:"torrentgroup"`
	VanityHouse  bool                        `json:"vanityHouse"`
}

type GazelleArtistTorrentGroup struct {
	Artists []struct {
		Aliasid int    `json:"aliasid"`
		ID      int    `json:"id"`
		Name    string `json:"name"`
	} `json:"artists"`
	ExtendedArtists      ExtendedArtists        `json:"extendedArtists"`
	GroupCatalogueNumber string                 `json:"groupCatalogueNumber"`
	GroupCategoryID      IntOrString            `json:"groupCategoryID"`
	GroupID              int                    `json:"groupId"`
	GroupName            string                 `json:"groupName"`
	GroupRecordLabel     string                 `json:"groupRecordLabel"`
	GroupVanityHouse     bool                   `json:"groupVanityHouse"`
	GroupYear            int                    `json:"groupYear"`
	HasBookmarked        bool                   `json:"hasBookmarked"`
	ReleaseType          int                    `json:"releaseType"`
	Tags                 []string               `json:"tags"`
	Torrent              []GazelleArtistTorrent `json:"torrent"`
	WikiImage            string                 `json:"wikiImage"`
}

func (gatg *GazelleArtistTorrentGroup) HasWEBFLAC() bool {
	for _, t := range gatg.Torrent {
		if t.IsWEBFLAC() {
			return true
		}
	}
	return false
}

func (gatg *GazelleArtistTorrentGroup) HasPerfectFLAC() bool {
	for _, t := range gatg.Torrent {
		if t.IsPerfectFLAC() {
			return true
		}
	}
	return false
}

type GazelleArtistTorrent struct {
	Encoding            string `json:"encoding"`
	FileCount           int    `json:"fileCount"`
	Format              string `json:"format"`
	FreeTorrent         bool   `json:"freeTorrent"`
	GroupID             int    `json:"groupId"`
	HasCue              bool   `json:"hasCue"`
	HasFile             int    `json:"hasFile"`
	HasLog              bool   `json:"hasLog"`
	ID                  int    `json:"id"`
	Leechers            int    `json:"leechers"`
	LogScore            int    `json:"logScore"`
	Media               string `json:"media"`
	RemasterRecordLabel string `json:"remasterRecordLabel"`
	RemasterTitle       string `json:"remasterTitle"`
	RemasterYear        int    `json:"remasterYear"`
	Remastered          bool   `json:"remastered"`
	Scene               bool   `json:"scene"`
	Seeders             int    `json:"seeders"`
	Size                int    `json:"size"`
	Snatched            int    `json:"snatched"`
	Time                string `json:"time"`
}

func (gat *GazelleArtistTorrent) IsWEBFLAC() bool {
	return gat.Media == SourceWEB && gat.Format == FormatFLAC && !gat.Scene
}

func (gat *GazelleArtistTorrent) Is16bitWEBFLAC() bool {
	return gat.IsWEBFLAC() && gat.Encoding == QualityLossless
}

func (gat *GazelleArtistTorrent) Is24bitWEBFLAC() bool {
	return gat.IsWEBFLAC() && gat.Encoding == Quality24bitLossless
}

func (gat *GazelleArtistTorrent) Is100CD() bool {
	return gat.Media == SourceCD && gat.HasLog && gat.HasCue && gat.LogScore == 100
}

func (gat *GazelleArtistTorrent) IsPerfectFLAC() bool {
	// in this instance, only considering WEB & CD sources
	return gat.IsWEBFLAC() || gat.Is100CD()
}

type GazelleArtistResponse struct {
	Response GazelleArtist `json:"response"`
	Status   string        `json:"status"`
	Error    string        `json:"error"`
}

type GazelleRequest struct {
	BbDescription   string   `json:"bbDescription"`
	BitrateList     []string `json:"bitrateList"`
	CanEdit         bool     `json:"canEdit"`
	CanVote         bool     `json:"canVote"`
	CatalogueNumber string   `json:"catalogueNumber"`
	CategoryID      int      `json:"categoryId"`
	CategoryName    string   `json:"categoryName"`
	CommentPage     int      `json:"commentPage"`
	CommentPages    int      `json:"commentPages"`
	Comments        []struct {
		AddedTime      string `json:"addedTime"`
		AuthorID       int    `json:"authorId"`
		Avatar         string `json:"avatar"`
		Class          string `json:"class"`
		Comment        string `json:"comment"`
		Donor          bool   `json:"donor"`
		EditedTime     string `json:"editedTime"`
		EditedUserID   int    `json:"editedUserId"`
		EditedUsername string `json:"editedUsername"`
		Enabled        bool   `json:"enabled"`
		Name           string `json:"name"`
		PostID         int    `json:"postId"`
		Warned         bool   `json:"warned"`
	} `json:"comments"`
	Description  string   `json:"description"`
	FillerID     int      `json:"fillerId"`
	FillerName   string   `json:"fillerName"`
	FormatList   []string `json:"formatList"`
	Image        string   `json:"image"`
	IsBookmarked bool     `json:"isBookmarked"`
	IsFilled     bool     `json:"isFilled"`
	LastVote     string   `json:"lastVote"`
	LogCue       string   `json:"logCue"`
	MediaList    []string `json:"mediaList"`
	MinimumVote  int      `json:"minimumVote"`
	MusicInfo    struct {
		Artists   []Artist `json:"artists"`
		Composers []Artist `json:"composers"`
		Conductor []Artist `json:"conductor"`
		Dj        []Artist `json:"dj"`
		Producer  []Artist `json:"producer"`
		RemixedBy []Artist `json:"remixedBy"`
		With      []Artist `json:"with"`
	} `json:"musicInfo"`
	Oclc            string   `json:"oclc"`
	RecordLabel     string   `json:"recordLabel"`
	ReleaseName     string   `json:"releaseName"`
	ReleaseType     int      `json:"releaseType"`
	RequestID       int      `json:"requestId"`
	RequestorID     int      `json:"requestorId"`
	RequestorName   string   `json:"requestorName"`
	Tags            []string `json:"tags"`
	TimeAdded       string   `json:"timeAdded"`
	TimeFilled      string   `json:"timeFilled"`
	Title           string   `json:"title"`
	TopContributors []struct {
		Bounty   int    `json:"bounty"`
		UserID   int    `json:"userId"`
		UserName string `json:"userName"`
	} `json:"topContributors"`
	TorrentID   int `json:"torrentId"`
	TotalBounty int `json:"totalBounty"`
	VoteCount   int `json:"voteCount"`
	Year        int `json:"year"`
}

type GazelleRequestResponse struct {
	Response GazelleRequest `json:"response"`
	Status   string         `json:"status"`
	Error    string         `json:"error"`
}

type GazelleRequestSearch struct {
	CurrentPage int                   `json:"currentPage"`
	Pages       int                   `json:"pages"`
	Results     []RequestSearchResult `json:"results"`
}

type RequestSearchResult struct {
	Artists [][]struct {
		ID   IntOrString `json:"id"`
		Name string      `json:"name"`
	} `json:"artists"`
	BitrateList     []string `json:"bitrateList"`
	Bounty          int      `json:"bounty"`
	CatalogueNumber string   `json:"catalogueNumber"`
	CategoryID      int      `json:"categoryId"`
	CategoryName    string   `json:"categoryName"`
	Description     string   `json:"description"`
	FillerID        int      `json:"fillerId"`
	FillerName      string   `json:"fillerName"`
	FormatList      []string `json:"formatList"`
	Image           string   `json:"image"`
	IsFilled        bool     `json:"isFilled"`
	LastVote        string   `json:"lastVote"`
	LogCue          string   `json:"logCue"`
	MediaList       []string `json:"mediaList"`
	RecordLabel     string   `json:"recordLabel"`
	ReleaseType     string   `json:"releaseType"`
	RequestID       int      `json:"requestId"`
	RequestorID     int      `json:"requestorId"`
	RequestorName   string   `json:"requestorName"`
	TimeAdded       string   `json:"timeAdded"`
	TimeFilled      string   `json:"timeFilled"`
	Title           string   `json:"title"`
	TorrentID       int      `json:"torrentId"`
	VoteCount       int      `json:"voteCount"`
	Year            int      `json:"year"`
}

const (
	requestAny = "Any"
)

func (rsr *RequestSearchResult) IsWebLosslessFLAC() bool {
	isLossless := strslice.Contains(rsr.BitrateList, QualityLossless) || strslice.Contains(rsr.BitrateList, requestAny)
	isWeb := strslice.Contains(rsr.MediaList, SourceWEB) || strslice.Contains(rsr.MediaList, requestAny)
	isFlac := strslice.Contains(rsr.FormatList, FormatFLAC) || strslice.Contains(rsr.FormatList, requestAny)
	return isLossless && isWeb && isFlac
}

type GazelleRequestSearchResponse struct {
	Response GazelleRequestSearch `json:"response"`
	Status   string               `json:"status"`
	Error    string               `json:"error"`
}

type GazelleSearch struct {
	CurrentPage int `json:"currentPage"`
	Pages       int `json:"pages"`
	Results     []struct {
		Artist      string   `json:"artist"`
		Bookmarked  bool     `json:"bookmarked"`
		Cover       string   `json:"cover"`
		GroupID     int      `json:"groupId"`
		GroupName   string   `json:"groupName"`
		GroupTime   string   `json:"groupTime"`
		GroupYear   int      `json:"groupYear"`
		MaxSize     int      `json:"maxSize"`
		ReleaseType string   `json:"releaseType"`
		Tags        []string `json:"tags"`
		Torrents    []struct {
			Artists []struct {
				Aliasid int    `json:"aliasid"`
				ID      int    `json:"id"`
				Name    string `json:"name"`
			} `json:"artists"`
			CanUseToken             bool   `json:"canUseToken"`
			EditionID               int    `json:"editionId"`
			Encoding                string `json:"encoding"`
			FileCount               int    `json:"fileCount"`
			Format                  string `json:"format"`
			HasCue                  bool   `json:"hasCue"`
			HasLog                  bool   `json:"hasLog"`
			HasSnatched             bool   `json:"hasSnatched"`
			IsFreeleech             bool   `json:"isFreeleech"`
			IsNeutralLeech          bool   `json:"isNeutralLeech"`
			IsPersonalFreeleech     bool   `json:"isPersonalFreeleech"`
			Leechers                int    `json:"leechers"`
			LogScore                int    `json:"logScore"`
			Media                   string `json:"media"`
			RemasterCatalogueNumber string `json:"remasterCatalogueNumber"`
			RemasterTitle           string `json:"remasterTitle"`
			RemasterYear            int    `json:"remasterYear"`
			Remastered              bool   `json:"remastered"`
			Scene                   bool   `json:"scene"`
			Seeders                 int    `json:"seeders"`
			Size                    int    `json:"size"`
			Snatches                int    `json:"snatches"`
			Time                    string `json:"time"`
			TorrentID               int    `json:"torrentId"`
			VanityHouse             bool   `json:"vanityHouse"`
		} `json:"torrents"`
		TotalLeechers int  `json:"totalLeechers"`
		TotalSeeders  int  `json:"totalSeeders"`
		TotalSnatched int  `json:"totalSnatched"`
		VanityHouse   bool `json:"vanityHouse"`
	} `json:"results"`
}

type GazelleSearchResponse struct {
	Response GazelleSearch `json:"response"`
	Status   string        `json:"status"`
	Error    string        `json:"error"`
}

type GazelleInbox struct {
	CurrentPage int `json:"currentPage"`
	Messages    []struct {
		Avatar        string `json:"avatar"`
		ConvID        int    `json:"convId"`
		Date          string `json:"date"`
		Donor         bool   `json:"donor"`
		Enabled       bool   `json:"enabled"`
		ForwardedID   int    `json:"forwardedId"`
		ForwardedName string `json:"forwardedName"`
		SenderID      int    `json:"senderId"`
		Sticky        bool   `json:"sticky"`
		Subject       string `json:"subject"`
		Unread        bool   `json:"unread"`
		Username      string `json:"username"`
		Warned        bool   `json:"warned"`
	} `json:"messages"`
	Pages int `json:"pages"`
}

type GazelleInboxResponse struct {
	Response GazelleInbox `json:"response"`
	Status   string       `json:"status"`
	Error    string       `json:"error"`
}

type GazelleInboxConversation struct {
	ConvID   int `json:"convId"`
	Messages []struct {
		Avatar     string `json:"avatar"`
		BbBody     string `json:"bbBody"`
		Body       string `json:"body"`
		MessageID  int    `json:"messageId"`
		SenderID   int    `json:"senderId"`
		SenderName string `json:"senderName"`
		SentDate   string `json:"sentDate"`
	} `json:"messages"`
	Sticky  bool   `json:"sticky"`
	Subject string `json:"subject"`
}

type GazelleInboxConversationResponse struct {
	Response GazelleInboxConversation `json:"response"`
	Status   string                   `json:"status"`
	Error    string                   `json:"error"`
}

type GazelleCollage struct {
	CollageCategoryID   int      `json:"collageCategoryID"`
	CollageCategoryName string   `json:"collageCategoryName"`
	CreatorID           int      `json:"creatorID"`
	Deleted             bool     `json:"deleted"`
	Description         string   `json:"description"`
	HasBookmarked       bool     `json:"hasBookmarked"`
	ID                  int      `json:"id"`
	Locked              bool     `json:"locked"`
	MaxGroups           int      `json:"maxGroups"`
	MaxGroupsPerUser    int      `json:"maxGroupsPerUser"`
	Name                string   `json:"name"`
	SubscriberCount     int      `json:"subscriberCount"`
	TorrentGroupIDList  []string `json:"torrentGroupIDList"`
	Torrentgroups       []struct {
		CatalogueNumber string `json:"catalogueNumber"`
		CategoryID      string `json:"categoryId"`
		ID              string `json:"id"`
		MusicInfo       struct {
			Artists   []Artist `json:"artists"`
			Composers []Artist `json:"composers"`
			Conductor []Artist `json:"conductor"`
			Dj        []Artist `json:"dj"`
			Producer  []Artist `json:"producer"`
			RemixedBy []Artist `json:"remixedBy"`
			With      []Artist `json:"with"`
		} `json:"musicInfo"`
		Name        string `json:"name"`
		RecordLabel string `json:"recordLabel"`
		ReleaseType string `json:"releaseType"`
		TagList     string `json:"tagList"`
		Torrents    []struct {
			Encoding                string `json:"encoding"`
			FileCount               int    `json:"fileCount"`
			Format                  string `json:"format"`
			FreeTorrent             bool   `json:"freeTorrent"`
			HasCue                  bool   `json:"hasCue"`
			HasLog                  bool   `json:"hasLog"`
			Leechers                int    `json:"leechers"`
			LogScore                int    `json:"logScore"`
			Media                   string `json:"media"`
			RemasterCatalogueNumber string `json:"remasterCatalogueNumber"`
			RemasterRecordLabel     string `json:"remasterRecordLabel"`
			RemasterTitle           string `json:"remasterTitle"`
			RemasterYear            int    `json:"remasterYear"`
			Remastered              bool   `json:"remastered"`
			Reported                bool   `json:"reported"`
			Scene                   bool   `json:"scene"`
			Seeders                 int    `json:"seeders"`
			Size                    int    `json:"size"`
			Snatched                int    `json:"snatched"`
			Time                    string `json:"time"`
			Torrentid               int    `json:"torrentid"`
		} `json:"torrents"`
		VanityHouse string `json:"vanityHouse"`
		WikiImage   string `json:"wikiImage"`
		Year        string `json:"year"`
	} `json:"torrentgroups"`
}

func (gc *GazelleCollage) Anonymize() {
	gc.CreatorID = 0
}

type GazelleCollageResponse struct {
	Status   string         `json:"status"`
	Error    string         `json:"error"`
	Response GazelleCollage `json:"response"`
}

func MarshallResponse(response interface{}) ([]byte, error) {
	return json.MarshalIndent(response, "", "    ")
}

type UserSearch struct {
	CurrentPage int64 `json:"currentPage"`
	Pages       int64 `json:"pages"`
	Results     []struct {
		Avatar   string `json:"avatar"`
		Class    string `json:"class"`
		Donor    bool   `json:"donor"`
		Enabled  bool   `json:"enabled"`
		UserID   int    `json:"userId"`
		Username string `json:"username"`
		Warned   bool   `json:"warned"`
	} `json:"results"`
}

type UserSearchResponse struct {
	Status   string     `json:"status"`
	Error    string     `json:"error"`
	Response UserSearch `json:"response"`
}
