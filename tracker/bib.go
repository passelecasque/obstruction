package tracker

import (
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

const (
	EPUBFormat        = "15"
	ErrorUploadFailed = "upload failed"
)

type Bibliotik struct {
	Tracker
	RSSKey string
}

func NewBibliotik(domain, user, password, cookieValue, rssKey, userAgent string) (*Bibliotik, error) {
	t := &Bibliotik{RSSKey: rssKey}
	if err := t.Init("bib",
		domain,
		user,
		password,
		"id",
		cookieValue,
		strings.Replace(domain, "https://", ".", -1),
		"",
		userAgent,
		domain,
		false); err != nil {
		if rssKey != "" && errors.Is(err, errorLoginInfoRequired) {
			return t, nil
		}
		return nil, err
	}
	return t, nil
}

func (t *Bibliotik) Login() error {
	data, err := t.login()
	if err != nil {
		return err
	}
	// if using cookie, we need to parse at least one page to get the authkey
	if t.loginType != loginUserPassword {
		loginPageData, _, err := t.getRequestRateLimited(t.DomainURL)
		if err != nil {
			return err
		}
		data = string(loginPageData)
	}
	// getting authkey
	r := regexp.MustCompile(".*authkey=([[:alnum:]]{40}).*")
	if r.MatchString(data) {
		t.AuthKey = r.FindStringSubmatch(data)[1]
	} else {
		return errors.New("could not find authkey: " + errorLogIn)
	}
	return nil
}

func (t *Bibliotik) GetAnnounceURL() error {
	contents, _, err := t.getRequestRateLimited(t.DomainURL + "/upload")
	if err != nil {
		return err
	}
	// getting information
	r := regexp.MustCompile(`.*value="(http[s]?://[a-zA-Z\./]*\?passkey=[[:alnum:]]{32})".*`)
	if r.MatchString(string(contents)) {
		t.Passkey = r.FindStringSubmatch(string(contents))[1]
		return nil
	}
	return errors.New("could not find announce URL")
}

func (t *Bibliotik) generateSearchRequest(searchURL, author, title string) (string, error) {
	u, err := url.Parse(searchURL)
	if err != nil {
		return "", err
	}
	q := u.Query()
	// split authors if there are more than one
	// otherwise bib will only return hits if the authors are in the same order in its database.
	var authors []string
	for _, a := range strings.Split(author, ",") {
		authors = append(authors, `@creators `+a+``)
	}
	q.Set("search", fmt.Sprintf(`%s @title "%s"`, strings.Join(authors, " "), title))
	q.Set("for[]", EPUBFormat)
	u.RawQuery = q.Encode()
	return u.String(), nil
}

type Book struct {
	Title     string
	Link      string
	Authors   []string
	Publisher string
	Year      string
	IsRetail  bool
	Language  string
}

func (b *Book) String() string {
	var retail string
	if !b.IsRetail {
		retail = "[NON RETAIL]"
	}
	publisher := b.Publisher
	if b.Publisher == "" {
		publisher = "unknown publisher"
	}
	return fmt.Sprintf("%s (%s) %s [%s] %s\n\t%s", strings.Join(b.Authors, ", "), b.Year, b.Title, publisher, retail, b.Link)
}

func (t *Bibliotik) SearchBooks(args ...string) ([]*Book, error) {
	if len(args) != 2 {
		return nil, errors.New("expected 2 arguments: author, title")
	}
	if t.Client == nil {
		return nil, errors.New("not logged in")
	}
	authors := args[0]
	title := args[1]
	searchURL, err := t.generateSearchRequest(t.DomainURL+"/torrents/", authors, title)
	if err != nil {
		return nil, err
	}
	// search
	data, _, err := t.getRequestRateLimited(searchURL)
	if err != nil {
		return nil, err
	}
	// parse results
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(string(data)))
	if err != nil {
		return nil, err
	}
	// find torrents
	var hits []*Book
	doc.Find(".torrent").Each(func(i int, s *goquery.Selection) {
		b := &Book{}
		b.Title = s.Find(".title a").Text()
		link, _ := s.Find(".title a").Attr("href")
		b.Link = t.DomainURL + link
		s.Find(".authorLink").Each(func(i int, s *goquery.Selection) {
			b.Authors = append(b.Authors, s.Text())
		})
		b.Publisher = s.Find(".publisherLink").Text()
		b.Year = s.Find(".torYear").Text()
		if s.Find(".torRetail").Text() != "" {
			b.IsRetail = true
		}
		b.Language = s.Find(".torLanguage").Text()
		if len(b.Language) > 2 {
			// stripping [ ]
			b.Language = b.Language[1 : len(b.Language)-1]
		}
		if b.Language == "" {
			b.Language = "English"
		}
		hits = append(hits, b)
	})
	return hits, nil
}

func (t *Bibliotik) Search(args ...string) ([]string, error) {
	books, err := t.SearchBooks(args...)
	if err != nil {
		return []string{}, err
	}
	var hits []string
	for _, b := range books {
		hits = append(hits, b.String())
	}
	return hits, nil
}

func (t *Bibliotik) Upload(uf *BibUploadForm) error {
	if uf == nil {
		return errors.New("invalid upload form")
	}
	// prepare request
	req, err := uf.createRequest(t.DomainURL+"/upload/ebooks", t.AuthKey)
	if err != nil {
		return errors.New("could not prepare upload form")
	}
	// adding headers
	req.Header.Add("User-Agent", t.UserAgent)
	if t.SessionCookie != nil {
		req.AddCookie(t.SessionCookie)
	}

	// submit the request
	resp, err := t.Client.Do(req)
	if err != nil {
		return errors.New("could not upload torrent")
	}
	defer resp.Body.Close()

	// check what URL the response came from
	finalURL := resp.Request.URL.String()
	if finalURL == t.DomainURL+"/upload/ebooks" {
		// response was from the upload form, meaning the fields contained errors and were rejected.
		return errors.New(ErrorUploadFailed)
	}
	fmt.Println("Success! Uploaded to " + finalURL)
	return err
}
