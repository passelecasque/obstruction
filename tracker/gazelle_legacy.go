package tracker

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
)

func (t *Gazelle) legacyFillRequest(requestID, torrentID int) (*FillRequest, error) {
	// setting up the form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "takefill"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("requestid", strconv.Itoa(requestID)))
	errs = append(errs, w.WriteField("link", t.DomainURL+"/torrents.php?torrentid="+strconv.Itoa(torrentID)))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	editURL := t.DomainURL + "/requests.php?action=view&id=" + strconv.Itoa(requestID)
	data, _, err := t.postRequestRateLimited(editURL, &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return nil, err
	}

	// check it's now correctly filled
	r := regexp.MustCompile(filledRequestPattern)
	if r.MatchString(string(data)) {
		hits := r.FindStringSubmatch(string(data))
		if len(hits) != 3 {
			return nil, errors.New("cannot confirm if request is filled")
		}
		if hits[1] == strconv.Itoa(torrentID) && hits[2] == t.User {
			return &FillRequest{Torrentid: torrentID, Requestid: requestID, Fillerid: t.UserID, Fillername: t.User}, nil
		}
		return nil, fmt.Errorf("request filled with torrent %s by %s", hits[1], hits[2])
	}
	return nil, errors.New("request could not be filled")
}

func (t *Gazelle) legacyGetLogScore(logPath string) (*LogChecker, error) {
	if !fs.FileExists(logPath) {
		return nil, errors.New("Log does not exist: " + logPath)
	}
	// setting up the form
	buffer := new(bytes.Buffer)
	w := multipart.NewWriter(buffer)

	// write to "log" input
	f, err := os.Open(logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}
	defer f.Close()
	fw, err := w.CreateFormFile("log", logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotCreateForm)
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}

	// some forms use "logfiles[]", so adding the same data to that input name
	// both will be sent, each tracker will pick up what they want
	f2, err := os.Open(logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}
	defer f2.Close()
	fw2, err := w.CreateFormFile("logfiles[]", logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotCreateForm)
	}
	if _, err = io.Copy(fw2, f2); err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}

	// other inputs
	var errs []error
	errs = append(errs, w.WriteField("submit", "true"))
	errs = append(errs, w.WriteField("action", "takeupload"))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	// posting data
	data, _, err := t.postRequestRateLimited(t.DomainURL+"/logchecker.php", buffer, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return nil, err
	}

	// getting log score
	lc := LogChecker{}
	returnData := string(data)
	r := regexp.MustCompile(logScorePattern)
	if r.MatchString(returnData) {
		// is int by construction
		score, _ := strconv.Atoi(r.FindStringSubmatch(returnData)[1])
		lc.Score = score
		lc.Issues = append(lc.Issues, "Log correctly parsed.")
	}
	if strings.Contains(returnData, "Your log has failed.") {
		lc.Issues = append(lc.Issues, "Log rejected")
	} else if strings.Contains(returnData, "This too shall pass.") {
		lc.Score = 100
		lc.Issues = append(lc.Issues, "Log checks out, at least Silver")
	}
	if len(lc.Issues) == 0 {
		return nil, errors.New("Could not find score")
	}
	return &lc, nil
}
