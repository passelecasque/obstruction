package tracker

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/ui"
)

func (t *Gazelle) GetArtist(artistID int) (*GazelleArtist, error) {
	var s GazelleArtistResponse
	err := t.unmarshalAPICall("action=artist&id="+strconv.Itoa(artistID), &s)
	return &s.Response, err
}

func (t *Gazelle) GetArtistFromName(artistName string) (*GazelleArtist, error) {
	q := url.Values{}
	q.Set("action", "artist")
	q.Set("artistname", artistName)
	var s GazelleArtistResponse
	err := t.unmarshalAPICall(q.Encode(), &s)
	return &s.Response, err
}

func (t *Gazelle) GetTorrent(torrentID int) (*GazelleTorrent, error) {
	var s GazelleTorrentResponse
	err := t.unmarshalAPICall("action=torrent&id="+strconv.Itoa(torrentID), &s)
	if err != nil {
		isDeleted, deletedText, errCheck := t.IsTorrentDeleted(torrentID)
		if errCheck != nil {
			logthis.Error(errors.Wrap(errCheck, "could not get information from site log"), logthis.VERBOSE)
		} else if isDeleted {
			logthis.Info(ui.Red(deletedText), logthis.NORMAL)
		}
		return nil, errors.Wrap(err, errorJSONAPI)
	}
	return &s.Response, err
}

func (t *Gazelle) GetTorrentByHash(hash string) (*GazelleTorrent, error) {
	var s GazelleTorrentResponse
	err := t.unmarshalAPICall("action=torrent&hash="+strings.ToUpper(hash), &s)
	return &s.Response, err
}

func (t *Gazelle) GetTorrentGroup(torrentGroupID int) (*GazelleTorrentGroup, error) {
	var s GazelleTorrentGroupResponse
	err := t.unmarshalAPICall("action=torrentgroup&id="+strconv.Itoa(torrentGroupID), &s)
	return &s.Response, err
}

// GetRequest information from the API
func (t *Gazelle) GetRequest(requestID int) (*GazelleRequest, error) {
	var s GazelleRequestResponse
	err := t.unmarshalAPICall("action=request&id="+strconv.Itoa(requestID), &s)
	return &s.Response, err
}

// SearchRequest information from the API
func (t *Gazelle) SearchRequest(artist, title string) (*GazelleRequestSearch, error) {
	var s GazelleRequestSearchResponse
	// media[]=7 == WEB, formats[]=1 == FLAC, &bitrates[]=8 == 16bit Lossless
	// although bitrates=8 also catches 24bit lossless
	err := t.unmarshalAPICall("action=requests&search="+url.QueryEscape(artist+" "+title)+"&media[]=7&formats[]=1&bitrates[]=8", &s)
	return &s.Response, err
}

// Search for releases from the API
func (t *Gazelle) Search(artist, title string) (*GazelleSearch, error) {
	var s GazelleSearchResponse
	err := t.unmarshalAPICall("action=browse&artistname="+url.QueryEscape(artist)+"&groupname="+url.QueryEscape(title), &s)
	return &s.Response, err
}

func (t *Gazelle) GetUserStats(userID int) (*GazelleUserStats, error) {
	var s GazelleUserStatsResponse
	err := t.unmarshalAPICall("action=user&id="+strconv.Itoa(userID), &s)
	return &s.Response, err
}

func (t *Gazelle) GetIndex() (*GazelleIndex, error) {
	var s GazelleIndexResponse
	err := t.unmarshalAPICall("action=index", &s)
	return &s.Response, err
}

func (t *Gazelle) GetInbox(pageNumber int, inboxType string, sort bool, search, searchType string) (*GazelleInbox, error) {
	/*
		page - page number to display (default: 1)
		type - one of: inbox or sentbox (default: inbox)
		sort - if set to unread then unread messages come first
		search - filter messages by search string
		searchtype - one of: subject, message, user
	*/
	q := url.Values{}
	q.Set("action", "inbox")
	q.Set("page", strconv.Itoa(pageNumber))
	q.Set("type", inboxType)
	q.Set("sort", fmt.Sprintf("%v", sort))
	q.Set("search", search)
	q.Set("searchtype", searchType)
	var s GazelleInboxResponse
	err := t.unmarshalAPICall(q.Encode(), &s)
	return &s.Response, err
}

func (t *Gazelle) GetInboxConversation(convID int) (*GazelleInboxConversation, error) {
	/*
		id - id of the message to display
	*/
	q := url.Values{}
	q.Set("action", "inbox")
	q.Set("type", "viewconv")
	q.Set("id", strconv.Itoa(convID))
	var s GazelleInboxConversationResponse
	err := t.unmarshalAPICall(q.Encode(), &s)
	return &s.Response, err
}

func (t *Gazelle) GetCollage(id int) (*GazelleCollage, error) {
	/*
		id - id of the collage
	*/
	q := url.Values{}
	q.Set("action", "collage")
	q.Set("id", strconv.Itoa(id))
	var s GazelleCollageResponse
	err := t.unmarshalAPICall(q.Encode(), &s)
	return &s.Response, err
}

func (t *Gazelle) UserSearch(name string) (*UserSearch, error) {
	/*
		name - user name
	*/
	q := url.Values{}
	q.Set("action", "usersearch")
	q.Set("search", name)
	var s UserSearchResponse
	err := t.unmarshalAPICall(q.Encode(), &s)
	return &s.Response, err
}
