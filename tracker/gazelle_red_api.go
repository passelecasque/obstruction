package tracker

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
)

var (
	errImprovedAPIOnly = errors.New("not available with this version of the Gazelle API")
)

func (t *Gazelle) GetLogScore(logPath string) (*LogChecker, error) {
	if t.loginType == loginUserPassword || t.loginType == loginSessionCookie {
		return t.legacyGetLogScore(logPath)
	}
	if !fs.FileExists(logPath) {
		return nil, errors.New("Log does not exist: " + logPath)
	}
	// setting up the form
	buffer := new(bytes.Buffer)
	w := multipart.NewWriter(buffer)
	// write to "log" input
	f, err := os.Open(logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}
	defer f.Close()
	fw, err := w.CreateFormFile("log", logPath)
	if err != nil {
		return nil, errors.Wrap(err, errorCouldNotCreateForm)
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, errors.Wrap(err, errorCouldNotReadLog)
	}
	// other inputs
	var errs []error
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	// posting data
	var lc LogCheckerResponse
	err = t.unmarshalPOSTAPICall("action=logchecker", buffer, w.FormDataContentType(), true, &lc)
	return &lc.Response, err
}

func (t *Gazelle) GetUserTorrents(torrentType string, userID, number, offset int) (*UserTorrents, error) {
	if !t.improvedAPI {
		return nil, errImprovedAPIOnly
	}
	if !IsValidTorrentType(torrentType) {
		return nil, errors.New("invalid torrent type")
	}
	var s UserTorrentsResponse
	endpoint := fmt.Sprintf("action=user_torrents&id=%d&type=%s&limit=%d&offset=%d", userID, torrentType, number, offset)
	err := t.unmarshalAPICall(endpoint, &s)
	return &s.Response, err
}

func (t *Gazelle) Download(id int, useFLToken bool, destinationFolder, torrentFilename string) error {
	if torrentFilename == "" {
		torrentFilename = fs.SanitizePath(t.Name) + "_id" + strconv.Itoa(id) + torrentExt
	}
	/*
		id - id of the collage
		usetoken - if 1, use freeleech token
	*/
	var torrentURL string
	q := url.Values{}
	q.Set("action", "download")
	q.Set("id", strconv.Itoa(id))
	if useFLToken {
		q.Set("usetoken", "1")
	}
	// the new download endpoint requires API key usage
	if t.loginType == loginAPIKey || t.loginType == loginAPIKeyAndSessionCookie {
		torrentURL = t.apiURL + q.Encode()
	} else {
		torrentURL = t.DomainURL + "/torrents.php?" + q.Encode()
	}
	return t.downloadTorrentFile(torrentURL, torrentFilename, destinationFolder)
}

func (t *Gazelle) FillRequest(requestID, torrentID int) (*FillRequest, error) {
	/*
		requestid - id of the request
		torrentid - id of the torrent
	*/
	if t.loginType == loginSessionCookie || t.loginType == loginUserPassword {
		return t.legacyFillRequest(requestID, torrentID)
	}
	// GET
	q := url.Values{}
	q.Set("action", "requestfill")
	// POST
	post := url.Values{}
	post.Set("requestid", strconv.Itoa(requestID))
	post.Set("torrentid", strconv.Itoa(torrentID))

	var s FillRequestResponse
	err := t.unmarshalPOSTAPICall(q.Encode(), strings.NewReader(post.Encode()), "application/x-www-form-urlencoded", true, &s)
	if err != nil {
		return nil, err
	}
	return &s.Response, err
}

func (t *Gazelle) UploadMusic(torrentPath string, logFile string, metadata *GazelleTorrent, requestID int) (*UploadResult, error) {
	// GET
	q := url.Values{}
	q.Set("action", "upload")

	// POST
	// is this a new torrent group or a new format?
	usesExistingGroupTorrent := metadata.Group.ID != 0

	// setting up the form
	buffer := new(bytes.Buffer)
	w := multipart.NewWriter(buffer)
	// adding the torrent file
	f, err := os.Open(torrentPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fw, err := w.CreateFormFile("file_input", filepath.Base(torrentPath))
	if err != nil {
		return nil, err
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}

	// adding the log file
	if logFile != "" {
		logF, err := os.Open(logFile)
		if err != nil {
			return nil, err
		}
		defer logF.Close()

		fw, err := w.CreateFormFile("logfiles[]", filepath.Base(logFile))
		if err != nil {
			return nil, err
		}
		if _, err = io.Copy(fw, logF); err != nil {
			return nil, err
		}
	}

	var errs []error
	// adding fields if filling the upload form
	if t.loginType == loginSessionCookie || t.loginType == loginUserPassword {
		errs = append(errs, w.WriteField("submit", "true"))
		errs = append(errs, w.WriteField("auth", t.AuthKey))
		errs = append(errs, w.WriteField("remaster", "true"))
	}

	if usesExistingGroupTorrent {
		// if there is a known torrent group ID, associate the upload with it
		errs = append(errs, w.WriteField("groupid", strconv.Itoa(metadata.Group.ID)))
	} else {
		// categories: 0 music, 2 ebooks
		errs = append(errs, w.WriteField("type", "0"))
		// posting as many fields as there are artists in each category
		for _, a := range metadata.Group.MusicInfo.Artists {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "1"))
		}
		for _, a := range metadata.Group.MusicInfo.With {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "2"))
		}
		for _, a := range metadata.Group.MusicInfo.RemixedBy {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "3"))
		}
		for _, a := range metadata.Group.MusicInfo.Composers {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "4"))
		}
		for _, a := range metadata.Group.MusicInfo.Conductor {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "5"))
		}
		for _, a := range metadata.Group.MusicInfo.Dj {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "6"))
		}
		for _, a := range metadata.Group.MusicInfo.Producer {
			errs = append(errs, w.WriteField("artists[]", a.Name))
			errs = append(errs, w.WriteField("importance[]", "7"))
		}
		errs = append(errs, w.WriteField("title", metadata.Group.Name))
		errs = append(errs, w.WriteField("year", strconv.Itoa(metadata.Group.Year)))
		errs = append(errs, w.WriteField("releasetype", strconv.Itoa(metadata.Group.ReleaseType)))
		errs = append(errs, w.WriteField("tags", strings.Join(metadata.Group.Tags, ",")))
		errs = append(errs, w.WriteField("image", metadata.Group.WikiImage))
		errs = append(errs, w.WriteField("album_desc", metadata.Group.WikiBody))
	}
	// torrent-specific fields
	errs = append(errs, w.WriteField("remaster_year", strconv.Itoa(metadata.Torrent.RemasterYear)))
	errs = append(errs, w.WriteField("remaster_title", metadata.Torrent.RemasterTitle))
	errs = append(errs, w.WriteField("remaster_record_label", metadata.Torrent.RemasterRecordLabel))
	errs = append(errs, w.WriteField("remaster_catalogue_number", metadata.Torrent.RemasterCatalogueNumber))
	// NOTE: scene not supported
	errs = append(errs, w.WriteField("format", metadata.Torrent.Format))
	errs = append(errs, w.WriteField("bitrate", metadata.Torrent.Encoding))
	errs = append(errs, w.WriteField("media", metadata.Torrent.Media))
	errs = append(errs, w.WriteField("release_desc", metadata.Torrent.Description))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	// using API endpoint if possible
	if t.loginType == loginAPIKeyAndSessionCookie || t.loginType == loginAPIKey {
		// filling request when uploading
		if requestID != 0 {
			errs = append(errs, w.WriteField("requestid", strconv.Itoa(requestID)))
		}
		var s UploadResultResponse
		err = t.unmarshalPOSTAPICall(q.Encode(), buffer, w.FormDataContentType(), true, &s)
		if err != nil {
			return nil, err
		}
		return &s.Response, err
	}

	// falling back to upload form
	uploadURL := t.DomainURL + "/upload.php"
	var additions []string
	if usesExistingGroupTorrent {
		additions = append(additions, "groupid="+strconv.Itoa(metadata.Group.ID))
	}
	if requestID != 0 {
		additions = append(additions, "requestid="+strconv.Itoa(requestID))
	}
	if len(additions) != 0 {
		uploadURL += "?" + strings.Join(additions, "&")
	}
	respData, respURL, err := t.postRequestRateLimited(uploadURL, buffer, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return nil, err
	}

	// returned URL should be t.DomainURL+ /torrents.php?id= + torrentGroupID
	if respURL == uploadURL {
		logthis.Info(string(respData), logthis.VERBOSESTEST)
		return nil, errors.New("edition page returned, modifications rejected")
	}
	// extracting the torrent group ID
	r := regexp.MustCompile(uploadSuccessfulPattern)
	if r.MatchString(respURL) {
		// cannot error out by construction
		torrentGroupID, _ := strconv.Atoi(r.FindStringSubmatch(respURL)[1])
		torrentID, err := t.GetLastUploadedToTorrentGroup(torrentGroupID)
		if err != nil {
			return nil, errors.New("could not find torrentID for this upload")
		}
		return &UploadResult{GroupID: torrentGroupID, TorrentID: torrentID}, nil
	}
	return nil, errors.New("upload seemingly rejected, could not find confirmation and torrent group id")
}

func (t *Gazelle) AddGroupTags(torrentGroupID int, tags []string) (*AddTag, error) {
	if t.loginType == loginSessionCookie || t.loginType == loginUserPassword {
		return t.legacyAddGroupTags(torrentGroupID, tags)
	}

	// removing duplicates
	strslice.RemoveDuplicates(&tags)
	// get values
	q := url.Values{}
	q.Set("action", "addtag")
	// post values
	post := url.Values{}
	post.Set("groupid", strconv.Itoa(torrentGroupID))
	post.Set("tagname", strings.Join(tags, ","))
	post.Encode()

	// posting data
	var lc AddTagResponse
	err := t.unmarshalPOSTAPICall(q.Encode(), strings.NewReader(post.Encode()), "application/x-www-form-urlencoded", true, &lc)
	return &lc.Response, err
}
