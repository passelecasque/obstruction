package tracker

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtils(t *testing.T) {
	fmt.Println("+ Testing utils...")
	check := assert.New(t)

	id, err := ExtractGroupID("2321")
	check.Nil(err)
	check.Equal(2321, id)
	id, err = ExtractGroupID("https://redasdsd.sch/torrents.php?id=1425718")
	check.Nil(err)
	check.Equal(1425718, id)
	id, err = ExtractGroupID("https://rooooh/torrents.php?id=1425718&torrentid=3034105#torrent3034105")
	check.Nil(err)
	check.Equal(1425718, id)
	id, err = ExtractGroupID("https://rooooh/torrents.php?torrentid=3034105&id=1425718&#torrent3034105")
	check.Nil(err)
	check.Equal(1425718, id)
	id, err = ExtractGroupID("255juj321")
	check.NotNil(err)
	check.Equal(-1, id)
	id, err = ExtractGroupID("https://rooooh/torrents.php?torrentid=3034105")
	check.NotNil(err)
	check.Equal(-1, id)

	id, err = ExtractTorrentID("2321")
	check.Nil(err)
	check.Equal(2321, id)
	id, err = ExtractTorrentID("https://redasdsd.sch/torrents.php?id=1425718")
	check.NotNil(err)
	check.Equal(-1, id)
	id, err = ExtractTorrentID("https://rooooh/torrents.php?id=1425718&torrentid=3034105#torrent3034105")
	check.Nil(err)
	check.Equal(3034105, id)
	id, err = ExtractTorrentID("https://rooooh/torrents.php?torrentid=3034105&id=1425718&#torrent3034105")
	check.Nil(err)
	check.Equal(3034105, id)
	id, err = ExtractTorrentID("255juj321")
	check.NotNil(err)
	check.Equal(-1, id)
	id, err = ExtractTorrentID("https://rooooh/torrents.php?torrentid=3034105")
	check.Nil(err)
	check.Equal(3034105, id)

}
