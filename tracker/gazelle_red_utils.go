package tracker

func (t *Gazelle) GetSnatchedUserTorrents(userID, number, offset int) (*UserTorrents, error) {
	return t.GetUserTorrents(UserTorrentsSnatched, userID, number, offset)
}

func (t *Gazelle) GetLeechingUserTorrents(userID, number, offset int) (*UserTorrents, error) {
	return t.GetUserTorrents(UserTorrentsLeeching, userID, number, offset)
}

func (t *Gazelle) GetUploadedUserTorrents(userID, number, offset int) (*UserTorrents, error) {
	return t.GetUserTorrents(UserTorrentsUploaded, userID, number, offset)
}

func (t *Gazelle) GetSeedingUserTorrents(userID, number, offset int) (*UserTorrents, error) {
	return t.GetUserTorrents(UserTorrentsSeeding, userID, number, offset)
}

func (t *Gazelle) GetAllUserTorrents(torrentType string, userID int) (*UserTorrents, error) {
	const limit = 10000
	var currentOffset int
	userTorrents := new(UserTorrents)
	lastNumberOfResults := limit
	for lastNumberOfResults == limit {
		torrents, err := t.GetUserTorrents(torrentType, userID, limit, currentOffset)
		if err != nil {
			return nil, err
		}
		switch torrentType {
		case UserTorrentsSnatched:
			userTorrents.Snatched = append(userTorrents.Snatched, torrents.Snatched...)
			lastNumberOfResults = len(torrents.Snatched)
		case UserTorrentsLeeching:
			userTorrents.Leeching = append(userTorrents.Leeching, torrents.Leeching...)
			lastNumberOfResults = len(torrents.Leeching)
		case UserTorrentsUploaded:
			userTorrents.Uploaded = append(userTorrents.Uploaded, torrents.Uploaded...)
			lastNumberOfResults = len(torrents.Uploaded)
		case UserTorrentsSeeding:
			userTorrents.Seeding = append(userTorrents.Seeding, torrents.Seeding...)
			lastNumberOfResults = len(torrents.Seeding)
		}
		currentOffset += lastNumberOfResults
	}
	return userTorrents, nil
}

func (t *Gazelle) GetAllSnatchedUserTorrents(userID int) (*UserTorrents, error) {
	return t.GetAllUserTorrents(UserTorrentsSnatched, userID)
}

func (t *Gazelle) GetAllLeechingUserTorrents(userID int) (*UserTorrents, error) {
	return t.GetAllUserTorrents(UserTorrentsLeeching, userID)
}

func (t *Gazelle) GetAllUploadedUserTorrents(userID int) (*UserTorrents, error) {
	return t.GetAllUserTorrents(UserTorrentsUploaded, userID)
}

func (t *Gazelle) GetAllSeedingUserTorrents(userID int) (*UserTorrents, error) {
	return t.GetAllUserTorrents(UserTorrentsSeeding, userID)
}
