package tracker

import (
	"time"

	"gitlab.com/catastrophic/assistance/strslice"
)

type GazelleTorrentResponse struct {
	Response GazelleTorrent `json:"response"`
	Status   string         `json:"status"`
	Error    string         `json:"error"`
}

type GazelleTorrent struct {
	Group struct {
		BbBody          string `json:"bbBody"`
		CatalogueNumber string `json:"catalogueNumber"`
		CategoryID      int    `json:"categoryId"`
		CategoryName    string `json:"categoryName"`
		ID              int    `json:"id"`
		IsBookmarked    bool   `json:"isBookmarked"`
		MusicInfo       struct {
			Artists   []Artist `json:"artists"`
			Composers []Artist `json:"composers"`
			Conductor []Artist `json:"conductor"`
			Dj        []Artist `json:"dj"`
			Producer  []Artist `json:"producer"`
			RemixedBy []Artist `json:"remixedBy"`
			With      []Artist `json:"with"`
		} `json:"musicInfo"`
		Name        string   `json:"name"`
		RecordLabel string   `json:"recordLabel"`
		ReleaseType int      `json:"releaseType"`
		Tags        []string `json:"tags"`
		Time        string   `json:"time"`
		VanityHouse bool     `json:"vanityHouse"`
		WikiBody    string   `json:"wikiBody"`
		WikiImage   string   `json:"wikiImage"`
		Year        int      `json:"year"`
	} `json:"group"`
	Torrent struct {
		Description             string `json:"description"`
		DynamicRange            string `json:"dynamicrange"`
		Encoding                string `json:"encoding"`
		FileCount               int    `json:"fileCount"`
		FileList                string `json:"fileList"`
		FilePath                string `json:"filePath"`
		Format                  string `json:"format"`
		Grade                   string `json:"grade"`
		HasCue                  bool   `json:"hasCue"`
		HasLog                  bool   `json:"hasLog"`
		HasSnatched             bool   `json:"has_snatched"`
		ID                      int    `json:"id"`
		InfoHash                string `json:"infoHash"`
		Leechers                int    `json:"leechers"`
		Lineage                 string `json:"lineage"`
		LogScore                int    `json:"logScore"`
		LossyMasterApproved     bool   `json:"lossyMasterApproved"`
		LossyWebApproved        bool   `json:"lossyWebApproved"`
		Matrixorrunout          string `json:"matrixorrunout"`
		Media                   string `json:"media"`
		RemasterCatalogueNumber string `json:"remasterCatalogueNumber"`
		RemasterRecordLabel     string `json:"remasterRecordLabel"`
		RemasterTitle           string `json:"remasterTitle"`
		RemasterYear            int    `json:"remasterYear"`
		Remastered              bool   `json:"remastered"`
		Reported                bool   `json:"reported"`
		SampleRate              string `json:"samplerate"`
		Scene                   bool   `json:"scene"`
		Seeders                 int    `json:"seeders"`
		Size                    int    `json:"size"`
		Snatched                int    `json:"snatched"`
		Time                    string `json:"time"`
		Trumpable               bool   `json:"trumpable"`
		UserID                  int    `json:"userId"`
		Username                string `json:"username"`
	} `json:"torrent"`
}

func (gt *GazelleTorrent) IsPerfectFLAC() bool {
	isFlac := gt.Is16bitFLAC() || gt.Is24bitFLAC()
	isPerfectCD := gt.Torrent.Media == SourceCD && gt.Torrent.HasCue && gt.Torrent.HasLog && gt.Torrent.LogScore == 100
	isNotScene := !gt.Torrent.Scene
	isWeb := gt.Torrent.Media == SourceWEB
	isNotTrumpable := !gt.Torrent.Trumpable
	return isFlac && isNotScene && isNotTrumpable && (isPerfectCD || isWeb)
}

func (gt *GazelleTorrent) IsFLAC() bool {
	return gt.Torrent.Format == FormatFLAC
}

func (gt *GazelleTorrent) Is16bitFLAC() bool {
	return gt.IsFLAC() && gt.Torrent.Encoding == QualityLossless
}

func (gt *GazelleTorrent) Is24bitFLAC() bool {
	return gt.IsFLAC() && gt.Torrent.Encoding == Quality24bitLossless
}

func (gt *GazelleTorrent) IsSmallerThan(maxSize int) bool {
	return gt.Torrent.Size < maxSize
}

func (gt *GazelleTorrent) IsSingleSeeded() bool {
	return gt.Torrent.Seeders == 1
}

func (gt *GazelleTorrent) IsAcceptable(excludedUploaders, excludedTags []string) bool {
	if strslice.Contains(excludedUploaders, gt.Torrent.Username) {
		return false
	}
	for _, et := range excludedTags {
		if strslice.Contains(gt.Group.Tags, et) {
			return false
		}
	}
	return true
}

func (gt *GazelleTorrent) MainArtists() []string {
	var artists []string
	for _, ga := range gt.Group.MusicInfo.Artists {
		artists = append(artists, ga.Name)
	}
	return artists
}

func (gt *GazelleTorrent) IsOlderThan(duration time.Duration) bool {
	layout := "2006-01-02 15:04:05"
	t, err := time.Parse(layout, gt.Torrent.Time)
	if err != nil {
		return false
	}
	return time.Since(t) > duration
}

func (gt *GazelleTorrent) WasUploadedBy(user string) bool {
	return gt.Torrent.Username == user
}
