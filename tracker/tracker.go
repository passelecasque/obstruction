package tracker

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"golang.org/x/net/publicsuffix"
)

const (
	errorNotLoggedIn    = "not logged in"
	errorSendingRequest = "error sending request"
	errorLogIn          = "Error logging in"

	torrentExt = ".torrent"

	// Gazelle usually only allows 5 API calls every 10s
	defaultAllowedAPICallsByPeriod = 5
	// making it 10.5s just to make sure.
	defaultAPICallsPeriodMs = 10500
)

const (
	loginUserPassword = iota
	loginSessionCookie
	loginAPIKey
	loginAPIKeyAndSessionCookie
)

var (
	errorLoginInfoRequired = errors.New("user/password, session cookie, or API key required")
)

// Tracker to do things with trackers.
type Tracker struct {
	sync.Mutex
	Name                  string
	DomainURL             string
	User                  string
	Password              string
	SessionCookie         *http.Cookie
	APIKey                string
	Client                *http.Client
	limiterAvailable      chan bool
	limiter               chan bool //  <- 1/tracker
	rateLimitRequired     bool
	rateLimitEnabled      bool
	rateLimitCallByPeriod int
	rateLimitPeriodMs     int
	rateLimitedLastCalls  []time.Time
	UserAgent             string // "name/version"
	Passkey               string
	LoginURL              string
	AuthKey               string
	loginType             int
}

func (t *Tracker) Init(name, domain, user, password, cookieID, cookieValue, cookieDomain, apiKey, userAgent, loginURL string, rateLimited bool) error {
	t.Name = name
	t.DomainURL = domain
	t.User = user
	t.Password = password
	t.APIKey = apiKey
	t.UserAgent = userAgent
	t.LoginURL = loginURL
	t.rateLimitRequired = rateLimited
	t.limiterAvailable = make(chan bool, 1)
	t.limiter = make(chan bool, defaultAllowedAPICallsByPeriod)
	t.rateLimitCallByPeriod = defaultAllowedAPICallsByPeriod
	t.rateLimitPeriodMs = defaultAPICallsPeriodMs
	t.rateLimitedLastCalls = make([]time.Time, t.rateLimitCallByPeriod)
	// initializing to current time
	for i := range t.rateLimitedLastCalls {
		t.rateLimitedLastCalls[i] = time.Now()
	}

	if cookieID != "" && cookieValue != "" {
		c := &http.Cookie{Name: cookieID, Value: cookieValue, Domain: cookieDomain}
		t.SessionCookie = c
	}

	switch {
	case t.APIKey != "" && cookieID != "" && cookieValue != "":
		t.loginType = loginAPIKeyAndSessionCookie
	case t.APIKey != "":
		t.loginType = loginAPIKey
	case cookieID != "" && cookieValue != "":
		t.loginType = loginSessionCookie
	case user != "" && password != "":
		t.loginType = loginUserPassword
	default:
		return errorLoginInfoRequired
	}
	return nil
}

func (t *Tracker) SetRateLimiter(numCall, period int) error {
	t.Lock()
	isRateLimited := t.rateLimitEnabled
	t.Unlock()
	if isRateLimited {
		return errors.New("rate limiter already started, cannot modify")
	}
	/*
		if numCall/period > defaultAllowedAPICallsByPeriod/defaultAPICallsPeriodMs {
			return errors.New("rate limiter cannot be lower than 5 calls every 10s")
		}
	*/
	t.rateLimitCallByPeriod = numCall
	t.rateLimitPeriodMs = period
	t.limiter = make(chan bool, t.rateLimitCallByPeriod)
	t.rateLimitedLastCalls = make([]time.Time, t.rateLimitCallByPeriod)
	// initializing to current time
	for i := range t.rateLimitedLastCalls {
		t.rateLimitedLastCalls[i] = time.Now()
	}
	return nil
}

func (t *Tracker) RateLimiter() {
	t.Lock()
	t.rateLimitEnabled = true
	t.Unlock()
	// fill the rate limiter the first time
	for i := 0; i < t.rateLimitCallByPeriod; i++ {
		t.limiter <- true
	}
	// limiter now available
	t.limiterAvailable <- true
	// every t.rateLimitPeriodMs, refill the limiter channel
	// this is an absolute time window, kept locally. It does not take into account when, in the window,
	// api calls are being made.
	// hence t.rateLimitedLastCalls, which keeps those timestamps, to make sure we're not sending too many requests as seen from the server.
	// if sending this for instance, the server would rate limit: |        xxxxx|xxxxx         |
	for range time.Tick(time.Millisecond * time.Duration(t.rateLimitPeriodMs)) {
		// making a copy of the timestamps to avoid race conditions
		savedTimestamps := make([]time.Time, t.rateLimitCallByPeriod)
		t.Lock()
		if copied := copy(savedTimestamps, t.rateLimitedLastCalls); copied != t.rateLimitCallByPeriod {
			logthis.Info("could not retrieve the timestamps of previous calls", logthis.NORMAL)
		}
		t.Unlock()
	Loop:
		// now considering the *ordered* slice of the last t.rateLimitCallByPeriod timestamps
		// if one was sent less than t.rateLimitPeriodMs ago, waiting for the difference to make sure we are allowed to make the request
		// |        xxxxx|xxxxx         |  => |        xxxxx|         xxxxx|
		for _, previousCallTime := range savedTimestamps {
			if time.Since(previousCallTime).Milliseconds() < int64(t.rateLimitPeriodMs) {
				// waiting until at least t.rateLimitPeriodMs after the oldest call
				time.Sleep(time.Duration(t.rateLimitPeriodMs)*time.Millisecond - time.Since(previousCallTime))
			}
			select {
			case t.limiter <- true:
			default:
				// if channel is full, do nothing and wait for the next tick
				break Loop
			}
		}
	}
}

func (t *Tracker) StartRateLimiter() {
	go t.RateLimiter()
	<-t.limiterAvailable
}

func (t *Tracker) waitForLimiter() error {
	if t.Client == nil {
		return errors.New(errorNotLoggedIn)
	}
	t.Lock()
	limited := t.rateLimitEnabled
	t.Unlock()
	if t.rateLimitRequired && !limited {
		return errors.New("rate limit required and not enabled")
	}
	if limited {
		// wait for rate limiter
		<-t.limiter
		// recording the timestamp of when the call was made, forgetting the oldest
		// this will be used when refilling the next time window
		t.Lock()
		t.rateLimitedLastCalls = append(t.rateLimitedLastCalls[1:], time.Now())
		t.Unlock()
	}
	return nil
}

func (t *Tracker) makeRequest(url, method string, body io.Reader, contentType string, apiKeySupported bool) ([]byte, string, error) {
	if t.Client == nil {
		return []byte{}, "", errors.New(errorNotLoggedIn)
	}

	var req *http.Request
	var err error
	switch method {
	case http.MethodGet:
		req, err = http.NewRequest(method, url, nil)
		if err != nil {
			return nil, "", err
		}
	case http.MethodPost:
		req, err = http.NewRequest(method, url, body)
		if err != nil {
			return nil, "", err
		}
		if contentType != "" {
			req.Header.Add("Content-Type", contentType)
		}
	default:
		return []byte{}, "", errors.New("unknown request method: " + method)
	}
	// adding user agent
	req.Header.Add("User-Agent", t.UserAgent)

	// adding api key authorization if possible, or session cookie if possible or if the endpoint does not support api keys
	switch {
	case t.loginType == loginAPIKey || (t.loginType == loginAPIKeyAndSessionCookie && apiKeySupported):
		req.Header.Add("Authorization", t.APIKey)
	case t.loginType == loginSessionCookie || (t.loginType == loginAPIKeyAndSessionCookie && !apiKeySupported):
		req.AddCookie(t.SessionCookie)
	}

	resp, err := t.Client.Do(req)
	if err != nil {
		logthis.Error(errors.Wrap(err, errorSendingRequest), logthis.NORMAL)
		return []byte{}, "", err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	// DEBUG
	//fmt.Println(url)
	//fmt.Println(string(data))
	//fmt.Println(resp.Status)

	if resp.StatusCode != http.StatusOK {
		return data, resp.Request.URL.String(), errors.New(resp.Status)
	}
	return data, resp.Request.URL.String(), err
}

func (t *Tracker) getRequest(url string) ([]byte, string, error) {
	return t.makeRequest(url, http.MethodGet, nil, "", true)
}

func (t *Tracker) postRequest(url string, b io.Reader, contentType string, apiKeySupported bool) ([]byte, string, error) {
	return t.makeRequest(url, http.MethodPost, b, contentType, apiKeySupported)
}

func (t *Tracker) getRequestRateLimited(url string) ([]byte, string, error) {
	if err := t.waitForLimiter(); err != nil {
		return []byte{}, "", err
	}
	return t.getRequest(url)
}

func (t *Tracker) postRequestRateLimited(url string, b io.Reader, contentType string, apiKeySupported bool) ([]byte, string, error) {
	if err := t.waitForLimiter(); err != nil {
		return []byte{}, "", err
	}
	return t.postRequest(url, b, contentType, apiKeySupported)
}

// Login and return the returned data, and error.
func (t *Tracker) login() (string, error) {
	// setting up http client
	options := cookiejar.Options{
		PublicSuffixList: publicsuffix.List,
	}
	jar, err := cookiejar.New(&options)
	if err != nil {
		logthis.Error(errors.Wrap(err, errorLogIn), logthis.NORMAL)
		return "", err
	}
	t.Client = &http.Client{Jar: jar, Timeout: time.Second * 30}

	// only actually logging in if we only have user/password.
	var req *http.Request
	if t.loginType == loginUserPassword {
		// log in using user/password
		form := url.Values{}
		form.Add("username", t.User)
		form.Add("password", t.Password)
		form.Add("keeplogged", "1")
		req, err = http.NewRequest(http.MethodPost, t.LoginURL, strings.NewReader(form.Encode()))
		if err != nil {
			logthis.Error(err, logthis.NORMAL)
			return "", err
		}
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		// setting user agent
		req.Header.Add("User-Agent", t.UserAgent)
		resp, err := t.Client.Do(req)
		if err != nil {
			logthis.Error(errors.Wrap(err, errorLogIn), logthis.NORMAL)
			return "", err
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			return "", errors.New(errorLogIn + ": Returned status: " + resp.Status)
		}
		data, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return "", errors.Wrap(err, errorLogIn)
		}
		if resp.Request.URL.String() == t.LoginURL {
			// if after sending the request we're still redirected to the login page, something went wrong.
			return string(data), errors.New(errorLogIn + ": Login page returned")
		}
		return string(data), nil
	}
	return "", nil
}
