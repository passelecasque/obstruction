package tracker

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/catastrophic/assistance/ui"
)

// BibUploadForm tracks all of the upload form fields.
type BibUploadForm struct {
	TorrentFileField  string
	TitleField        string
	EditorsField      string
	ContributorsField string
	TranslatorsField  string
	PublishersField   string
	PagesField        string
	AuthorsField      string
	FormatField       string // EPUB == "15"
	IsbnField         string
	TagsField         string
	DescriptionField  string
	RetailField       string // retail == "1"
	NotifyField       string // default "1"
	LanguageField     string // english == "1"
	YearField         string
	ImageField        string
	AnonymousField    string // default "0"
	SupplementsField  string // default "0"
}

func NewDefaultBibUploadForm() *BibUploadForm {
	return &BibUploadForm{FormatField: EPUBFormat, RetailField: "1", NotifyField: "1", LanguageField: "1", AnonymousField: "1", SupplementsField: "0"}
}

// ShowInfo returns a table with relevant information about a book.
func (uf *BibUploadForm) ShowInfo() string {
	var rows [][]string
	rows = append(rows, []string{"TorrentFile", uf.TorrentFileField})
	rows = append(rows, []string{"Title", uf.TitleField})
	rows = append(rows, []string{"Authors", uf.AuthorsField})
	rows = append(rows, []string{"Editors", uf.EditorsField})
	rows = append(rows, []string{"Contributors", uf.ContributorsField})
	rows = append(rows, []string{"Translators", uf.TranslatorsField})
	rows = append(rows, []string{"Publishers", uf.PublishersField})
	rows = append(rows, []string{"Isbn", uf.IsbnField})
	rows = append(rows, []string{"Pages", uf.PagesField})
	rows = append(rows, []string{"Year", uf.YearField})
	rows = append(rows, []string{"Format", uf.FormatField})
	rows = append(rows, []string{"Language", uf.LanguageField})
	rows = append(rows, []string{"Retail", uf.RetailField})
	rows = append(rows, []string{"Supplements", uf.SupplementsField})
	rows = append(rows, []string{"Tags", uf.TagsField})
	rows = append(rows, []string{"Image", uf.ImageField})
	rows = append(rows, []string{"Description", uf.DescriptionField})
	rows = append(rows, []string{"Notify", uf.NotifyField})
	rows = append(rows, []string{"Anonymous", uf.AnonymousField})
	return ui.TabulateRows(rows, "Info", "Book")
}

// CheckErrors and return the first non-nil one.
func CheckErrors(errs ...error) error {
	for _, err := range errs {
		if err != nil {
			return err
		}
	}
	return nil
}

func (uf *BibUploadForm) createRequest(uploadURL, authkey string) (*http.Request, error) {
	// setting up the form
	buffer := new(bytes.Buffer)
	w := multipart.NewWriter(buffer)
	// adding the torrent file
	f, err := os.Open(uf.TorrentFileField)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fw, err := w.CreateFormFile("TorrentFileField", filepath.Base(uf.TorrentFileField))
	if err != nil {
		return nil, err
	}
	if _, err = io.Copy(fw, f); err != nil {
		return nil, err
	}
	var errors []error
	errors = append(errors, w.WriteField("authkey", authkey))
	errors = append(errors, w.WriteField("upload", ""))
	errors = append(errors, w.WriteField("TitleField", uf.TitleField))
	errors = append(errors, w.WriteField("EditorsField", uf.EditorsField))
	errors = append(errors, w.WriteField("ContributorsField", uf.ContributorsField))
	errors = append(errors, w.WriteField("TranslatorsField", uf.TranslatorsField))
	errors = append(errors, w.WriteField("PublishersField", uf.PublishersField))
	errors = append(errors, w.WriteField("PagesField", uf.PagesField))
	errors = append(errors, w.WriteField("AuthorsField", uf.AuthorsField))
	errors = append(errors, w.WriteField("FormatField", uf.FormatField))
	errors = append(errors, w.WriteField("IsbnField", uf.IsbnField))
	errors = append(errors, w.WriteField("TagsField", uf.TagsField))
	errors = append(errors, w.WriteField("DescriptionField", uf.DescriptionField))
	errors = append(errors, w.WriteField("RetailField", uf.RetailField))
	errors = append(errors, w.WriteField("NotifyField", uf.NotifyField))
	errors = append(errors, w.WriteField("LanguageField", uf.LanguageField))
	errors = append(errors, w.WriteField("YearField", uf.YearField))
	errors = append(errors, w.WriteField("ImageField", uf.ImageField))
	errors = append(errors, w.WriteField("SupplementsField", uf.SupplementsField))
	errors = append(errors, w.WriteField("AnonymousField", uf.AnonymousField))
	if err := CheckErrors(errors...); err != nil {
		return nil, err
	}
	if err := w.Close(); err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, uploadURL, buffer)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", w.FormDataContentType())
	return req, err
}
