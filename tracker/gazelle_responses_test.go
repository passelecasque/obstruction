package tracker

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGazelleUtils(t *testing.T) {
	fmt.Println("+ Testing GazelleResponses Utils...")

	// setting up
	check := assert.New(t)

	check.Equal("Unknown value", GazelleReleaseType(128))
	check.Equal(ReleaseAlbum, GazelleReleaseType(1))

	check.Equal("UnF", ShortEncoding("NOPE"))
	check.Equal("FLAC24", ShortEncoding(Quality24bitLossless))

	check.Equal("DLX", ShortEdition("Deluxe Edition"))
	check.Equal("UnF", ShortEdition("UnF"))

	check.Equal(1, GazelleReleaseTypeByLabel(ReleaseAlbum))
	check.Equal(1, GazelleReleaseTypeByLabel("albUM"))
	check.Equal("Main", GazelleArtistRole(1))
	check.Equal(2, GazelleArtistRoleByLabel("Guest"))
}
