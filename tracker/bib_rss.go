package tracker

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/mmcdole/gofeed"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	regexpTitle = `(?U)^(.*)\s*-\s*(by (.*))?(edited by (.*))?(;\s*with contributions from (.*))? (\[(\d{4})\]) \[EPUB\]( \[RETAIL\])?$`
)

func (t *Bibliotik) SearchBooksByRSS(args ...string) ([]*Book, error) {
	if t.RSSKey == "" {
		return nil, errors.New("missing RSS key")
	}
	if len(args) != 2 {
		return nil, errors.New("expected 2 arguments: author, title")
	}

	// generating the RSS URL
	authors := args[0]
	title := args[1]
	searchURL, err := t.generateSearchRequest(fmt.Sprintf("%s/rss/torrents?rsskey=%s", t.DomainURL, t.RSSKey), authors, title)
	if err != nil {
		return nil, err
	}
	// getting and parsing the RSS
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	fp := gofeed.NewParser()
	feed, err := fp.ParseURLWithContext(searchURL, ctx)
	if err != nil {
		return nil, err
	}

	re := regexp.MustCompile(regexpTitle)
	var books []*Book
	for _, item := range feed.Items {
		b := &Book{}
		if !re.MatchString(item.Title) {
			return books, errors.New("cannot parse title " + item.Title)
		}
		//  splitting item.Title
		hits := re.FindAllStringSubmatch(item.Title, -1)
		if len(hits) == 0 {
			return books, errors.New("cannot parse title " + item.Title)
		}
		/*
			// for regexp debugging purposes
			for i, h := range hits {
				for j, p := range h {
					fmt.Println(i, j, p)
				}
			}
		*/

		// getting authors, editors, contributors
		b.Authors = strings.Split(strings.TrimSpace(hits[0][3]), ", ")
		b.Authors = append(b.Authors, strings.Split(strings.TrimSpace(hits[0][5]), ", ")...)
		b.Authors = append(b.Authors, strings.Split(strings.TrimSpace(hits[0][7]), ", ")...)
		b.Authors = strslice.Remove(b.Authors, "")
		b.Title = hits[0][1]
		b.Link = t.DomainURL + "/torrents/" + item.GUID
		b.Year = hits[0][9]
		b.IsRetail = hits[0][10] == " [RETAIL]"
		// defaulting to en
		b.Language = "en"
		books = append(books, b)
	}
	return books, nil
}
