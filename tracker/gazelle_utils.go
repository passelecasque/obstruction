package tracker

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/anacrolix/torrent/metainfo"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/intslice"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	regexpGroupURL   = `^https\:\/\/.*\/torrents\.php.*[\?\&]id=(\d*).*$`
	regexpTorrentURL = `^https\:\/\/.*\/torrents\.php.*[\?\&]torrentid=(\d*).*$`
)

func ExtractGroupID(urlOrID string) (int, error) {
	var groupID int
	var err error
	// if it's an int, converting
	if groupID, err = strconv.Atoi(urlOrID); err == nil {
		return groupID, nil
	}
	// else, trying to extract a group ID
	r := regexp.MustCompile(regexpGroupURL)
	hits := r.FindStringSubmatch(urlOrID)
	if len(hits) != 2 {
		return -1, errors.New("could not find ID")
	}
	groupID, _ = strconv.Atoi(hits[1])
	return groupID, nil
}

func ExtractTorrentID(urlOrID string) (int, error) {
	var torrentID int
	var err error
	// if it's an int, converting
	if torrentID, err = strconv.Atoi(urlOrID); err == nil {
		return torrentID, nil
	}
	// else, trying to extract an ID
	r := regexp.MustCompile(regexpTorrentURL)
	hits := r.FindStringSubmatch(urlOrID)
	if len(hits) != 2 {
		return -1, errors.New("could not find ID")
	}
	torrentID, _ = strconv.Atoi(hits[1])
	return torrentID, nil
}

func (t *Gazelle) GroupPermaLink(groupID int) string {
	return t.DomainURL + "/torrents.php?id=" + strconv.Itoa(groupID)
}

func (t *Gazelle) TorrentPermaLink(torrentID int) string {
	return t.DomainURL + "/torrents.php?torrentid=" + strconv.Itoa(torrentID)
}

func (t *Gazelle) RequestPermaLink(requestID int) string {
	return t.DomainURL + "/requests.php?action=view&id=" + strconv.Itoa(requestID)
}

func (t *Gazelle) CollagePermaLink(collageID int) string {
	return t.DomainURL + "/collages.php?id=" + strconv.Itoa(collageID)
}

func (t *Gazelle) ForumThreadPermaLink(threadID int) string {
	return t.DomainURL + "/forums.php?action=viewthread&threadid=" + strconv.Itoa(threadID)
}

func (t *Gazelle) GetLoggedUserStats() (*GazelleUserStats, error) {
	return t.GetUserStats(t.UserID)
}

func (t *Gazelle) GetUserStatsFromUsername(username string) (*GazelleUserStats, error) {
	gUserSearch, err := t.UserSearch(username)
	if err != nil {
		return nil, err
	}
	var userID int
	for _, c := range gUserSearch.Results {
		if strings.EqualFold(c.Username, username) {
			userID = c.UserID
			break
		}
	}
	if userID == 0 {
		return nil, errors.New("could not find user " + username)
	}
	return t.GetUserStats(userID)
}

func (t *Gazelle) GetTorrentFromFile(torrentPath string) (*GazelleTorrent, error) {
	m, err := metainfo.LoadFromFile(torrentPath)
	if err != nil {
		return nil, err
	}
	return t.GetTorrentByHash(m.HashInfoBytes().HexString())
}

func (t *Gazelle) GetLastUploadedToTorrentGroup(torrentGroupID int) (int, error) {
	tgt, err := t.GetTorrentGroup(torrentGroupID)
	if err != nil {
		return 0, err
	}
	var candidates []int
	for _, trt := range tgt.Torrents {
		if trt.Username == t.User {
			candidates = append(candidates, trt.ID)
		}
	}
	return intslice.Max(candidates), nil
}

func (t *Gazelle) IsTorrentDeleted(torrentID int) (bool, string, error) {
	if t.improvedAPI && t.APIKey != "" {
		return false, "", errors.New("cannot check the log when using an API key")
	}
	data, err := t.rateLimitedAPICall(t.DomainURL+"/log.php?search=Torrent+"+strconv.Itoa(torrentID), http.MethodGet, nil, "", false, false)
	if err != nil {
		return false, "", err
	}
	// getting deletion reason
	returnData := string(data)
	r := regexp.MustCompile(deletedFromTrackerPattern)
	if r.MatchString(returnData) {
		if r.FindStringSubmatch(returnData)[1] == strconv.Itoa(torrentID) {
			return true, "Site log: Torrent " + r.FindStringSubmatch(returnData)[1] + " " + r.FindStringSubmatch(returnData)[2], nil
		}
	}
	return false, "", nil
}

func (t *Gazelle) UploadMusicToExistingGroup(torrentGroupID int, torrentPath string, logFile string, metadata *GazelleTorrent, requestID int) (*UploadResult, error) {
	// most importantly, copying the torrent group ID
	metadata.Group.ID = torrentGroupID
	// upload
	upl, err := t.UploadMusic(torrentPath, logFile, metadata, requestID)
	if err != nil {
		return nil, err
	}
	if upl.GroupID != torrentGroupID {
		return upl, errors.New("somehow a new torrent group was created")
	}
	return upl, err
}

// downloadTorrentFile using its download URL.
func (t *Gazelle) downloadTorrentFile(torrentURL, torrentFile, destinationFolder string) error {
	if torrentURL == "" || torrentFile == "" {
		return errors.New(errorUnknownTorrentURL)
	}
	data, _, err := t.getRequestRateLimited(torrentURL)
	if err != nil {
		if len(data) != 0 {
			if responseErr := t.checkResponseStatus(data); responseErr != nil {
				return errors.New(responseErr.Error() + " (" + err.Error() + ")")
			}
		}
		return err
	}
	if err := ioutil.WriteFile(torrentFile, data, 0666); err != nil {
		return err
	}

	// get current directory
	currentPath, err := os.Getwd()
	if err != nil {
		return errors.Wrap(err, "cannot get current directory")
	}
	// get absolute destination directory
	destinationPath, err := filepath.Abs(destinationFolder)
	if err != nil {
		return errors.Wrap(err, "cannot find destination directory")
	}

	// check if destination folder is different
	saveHere, err := filepath.Match(destinationPath, currentPath)
	if err != nil {
		return errors.Wrap(err, "cannot compare destination directory with current")
	}
	// if it is, move the file
	if !saveHere {
		// move to relevant directory
		if err := fs.CopyFile(torrentFile, filepath.Join(destinationPath, torrentFile), false); err != nil {
			return errors.Wrap(err, errorCouldNotMoveTorrent)
		}
		// cleaning up
		if err := os.Remove(torrentFile); err != nil {
			logthis.Info(fmt.Sprintf(errorRemovingTempFile, torrentFile), logthis.VERBOSE)
		}
	}
	return nil
}

// CopyTagsFromRequest without doing any checks on whether the two are related.
func (t *Gazelle) CopyTagsFromRequest(requestID, torrentGroupID int, excludedTags []string) error {
	req, err := t.GetRequest(requestID)
	if err != nil {
		return err
	}
	strslice.RemoveDuplicatesCaseInsensitive(&req.Tags, excludedTags...)
	// nothing to copy from request
	if len(req.Tags) == 0 {
		return nil
	}
	resp, err := t.AddGroupTags(torrentGroupID, req.Tags)
	if err != nil {
		return err
	}

	if len(resp.Rejected) != 0 {
		var rejected []string
		for _, r := range resp.Rejected {
			rejected = append(rejected, r)
		}
		logthis.TimedInfo("Rejected tags from request: "+strings.Join(rejected, ","), logthis.NORMAL)
	}
	return err
}
