package tracker

import (
	"bytes"
	"fmt"
	"mime/multipart"
	"net/url"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/catastrophic/assistance/strslice"
)

const (
	uploadSuccessfulPattern = `torrents\.php\?id=(\d*)$`
	logScorePattern         = `(-?\d*)</span> \(out of 100\)</blockquote>`
	filledRequestPattern    = `(?mU)<td class="label">Filled</td>\s*<td>\s*<strong><a href="torrents\.php\?torrentid=(\d*)">Yes</a></strong>,\s*by user <a href="user\.php\?id=\d*">(.+)</a>`
)

func (t *Gazelle) EditTorrentDescriptionFromStruct(trt *GazelleTorrent) error {
	// NOTE: does not support UnknownRelease / scene
	// NOTE: note sure how it works with several remasters

	// setting up the form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "takeedit"))
	errs = append(errs, w.WriteField("submit", "true"))
	errs = append(errs, w.WriteField("type", "1"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("torrentid", fmt.Sprintf("%d", trt.Torrent.ID)))
	// TODO find a way to support more than 1 remaster
	// errs = append(errs, w.WriteField("groupremasters", "0"))
	errs = append(errs, w.WriteField("remaster_year", strconv.Itoa(trt.Torrent.RemasterYear)))
	errs = append(errs, w.WriteField("remaster_title", trt.Torrent.RemasterTitle))
	errs = append(errs, w.WriteField("remaster_record_label", trt.Torrent.RemasterRecordLabel))
	errs = append(errs, w.WriteField("remaster_catalogue_number", trt.Torrent.RemasterCatalogueNumber))
	errs = append(errs, w.WriteField("format", trt.Torrent.Format))
	errs = append(errs, w.WriteField("bitrate", trt.Torrent.Encoding))
	errs = append(errs, w.WriteField("media", trt.Torrent.Media))
	errs = append(errs, w.WriteField("release_desc", trt.Torrent.Description))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	editURL := t.DomainURL + "/torrents.php?action=edit&id=" + strconv.Itoa(trt.Torrent.ID)
	_, respURL, err := t.postRequestRateLimited(editURL, &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return err
	}

	// returned URL should be t.DomainURL+ /torrents.php?id= + torrentID
	if respURL == editURL || respURL == t.LoginURL {
		return errors.New("torrent edit page returned, modifications rejected")
	}
	return nil
}

func (t *Gazelle) EditTorrentDescription(torrentID int, description string) error {
	// getting previous information
	trt, err := t.GetTorrent(torrentID)
	if err != nil {
		return err
	}
	trt.Torrent.Description = description
	return t.EditTorrentDescriptionFromStruct(trt)
}

func (t *Gazelle) EditTorrentGroupDescription(torrentGroupID int, description, coverURL string) error {
	// getting previous information
	gtg, err := t.GetTorrentGroup(torrentGroupID)
	if err != nil {
		return nil
	}
	// setting up the form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "takegroupedit"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("groupid", strconv.Itoa(torrentGroupID)))
	if coverURL == "" {
		errs = append(errs, w.WriteField("image", gtg.Group.WikiImage))
	} else {
		errs = append(errs, w.WriteField("image", coverURL))
	}
	if description == "" {
		errs = append(errs, w.WriteField("body", gtg.Group.BbBody))
	} else {
		errs = append(errs, w.WriteField("body", description))
	}
	errs = append(errs, w.WriteField("releasetype", strconv.Itoa(gtg.Group.ReleaseType)))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	editURL := t.DomainURL + "/torrents.php?action=editgroup&groupid=" + strconv.Itoa(torrentGroupID)
	_, respURL, err := t.postRequestRateLimited(editURL, &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return err
	}
	// returned URL should be t.DomainURL+ /torrents.php?id= + torrentID
	if respURL == editURL || respURL == t.LoginURL {
		return errors.New("group edit page returned, modifications rejected")
	}
	return nil
}

func (t *Gazelle) legacyAddGroupTags(torrentGroupID int, tags []string) (*AddTag, error) {
	// removing duplicates
	strslice.RemoveDuplicates(&tags)

	// setting up the form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "add_tag"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("groupid", strconv.Itoa(torrentGroupID)))
	errs = append(errs, w.WriteField("tagname", strings.Join(tags, ",")))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	_, _, err := t.postRequestRateLimited(t.DomainURL+"/torrents.php?id="+strconv.Itoa(torrentGroupID), &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
	}
	// gazelle silently adds or rejects tags, no way to know if things went right or wrong.
	return &AddTag{}, err
}

func (t *Gazelle) AddGroupToCollage(collageID, torrentGroupID int) error {
	// setting up the form
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "add_torrent"))
	errs = append(errs, w.WriteField("submit", "Add"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("collageid", strconv.Itoa(collageID)))
	errs = append(errs, w.WriteField("url", t.GroupPermaLink(torrentGroupID)))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	_, respURL, err := t.postRequestRateLimited(t.CollagePermaLink(collageID), &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return err
	}
	// Note: this check doesn't really seem to work for some reason
	if respURL != t.CollagePermaLink(collageID) {
		return errors.New("collage page not returned, torrent group " + strconv.Itoa(torrentGroupID) + " was rejected")
	}
	return nil
}

func (t *Gazelle) AddForumPost(threadID int, text string) error {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "reply"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("thread", strconv.Itoa(threadID)))
	errs = append(errs, w.WriteField("body", text))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	_, respURL, err := t.postRequestRateLimited(t.ForumThreadPermaLink(threadID), &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return err
	}
	// URL should be the forum view + &page=XX
	if !strings.Contains(respURL, t.ForumThreadPermaLink(threadID)) {
		return errors.New("could not verify forum post was added to " + t.ForumThreadPermaLink(threadID))
	}
	return nil
}

const (
	reportEditingRequest             = "editing_request"
	reportDupe                       = "dupe"
	reportTrump                      = "trump"
	reportBadFilenamesTrump          = "file_trump"
	reportChecksumTrump              = "checksum_trump"
	reportBadFolderTrump             = "folder_trump"
	reportTagTrump                   = "tag_trump"
	reportVinylTrump                 = "vinyl_trump"
	reportAudienceRecording          = "audience"
	reportBadFilenames               = "filename"
	reportBadFolder                  = "folders_bad"
	reportBadOrNoTags                = "tags_lots"
	reportUncompressedLossless       = "uncompressed_lls"
	reportBonusTracksOnly            = "bonus_tracks"
	reportDisallowedFormat           = "format"
	reportDiscsMissing               = "discs_missing"
	reportDiscography                = "discog"
	reportEditedLog                  = "edited"
	reportInaccurateBitrate          = "bitrate"
	reportLogRescoreRequest          = "rescore"
	reportLossyMasterApprovalRequest = "lossyapproval"
	reportLossyWebApprovalRequest    = "lossywebapproval"
	reportLowBitrate                 = "low"
	reportMuttRip                    = "mutt"
	reportNoLineage                  = "lineage"
	reportOther                      = "other"
	reportForbiddenSource            = "source"
	reportSkipsEncodeErrors          = "skips"
	reportSpecificallyBanned         = "banned"
	reportTracksMissing              = "tracks_missing"
	reportTranscode                  = "transcode"
	reportUnsplitAlbumRip            = "single_track"
	reportUrgent                     = "urgent"
	reportUserCompilation            = "user_discog"
	reportWrongFormat                = "wrong_format"
	reportWrongMedia                 = "wrong_media"
)

func (t *Gazelle) ReportLossyWeb(torrentID int, link, text string) error {
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	var errs []error
	errs = append(errs, w.WriteField("action", "takereport"))
	errs = append(errs, w.WriteField("auth", t.AuthKey))
	errs = append(errs, w.WriteField("torrentid", strconv.Itoa(torrentID)))
	errs = append(errs, w.WriteField("categoryid", "1"))
	errs = append(errs, w.WriteField("type", reportLossyWebApprovalRequest))
	errs = append(errs, w.WriteField("proofimages", link))
	errs = append(errs, w.WriteField("extra", text))
	errs = append(errs, w.Close())
	logthis.ErrorIfNotNil(errs, logthis.VERBOSESTEST)

	getValues := encodeURLValues("action", "report", "id", strconv.Itoa(torrentID))
	_, respURL, err := t.postRequestRateLimited(t.DomainURL+"/reportsv2.php?"+getValues, &b, w.FormDataContentType(), false)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return err
	}
	// URL should be the torrent page, not the reporting form
	if strings.Contains(respURL, "reportsv2.php") {
		return errors.New("report rejected, report form sent back from server")
	}
	return nil
}

func encodeURLValues(args ...string) string {
	if len(args)%2 != 0 {
		logthis.Info("wrong number of arguments to make GET URL", logthis.VERBOSESTEST)
		return ""
	}
	q := url.Values{}
	for i := 0; i < len(args); i += 2 {
		q.Set(args[i], args[i+1])
	}
	return q.Encode()
}
